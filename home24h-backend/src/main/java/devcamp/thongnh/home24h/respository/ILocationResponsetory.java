package devcamp.thongnh.home24h.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import devcamp.thongnh.home24h.model.CLocation;

public interface ILocationResponsetory extends JpaRepository<CLocation,Long> {
    
}
