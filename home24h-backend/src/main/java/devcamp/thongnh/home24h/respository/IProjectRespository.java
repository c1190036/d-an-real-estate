package devcamp.thongnh.home24h.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import devcamp.thongnh.home24h.model.CProject;

public interface IProjectRespository extends JpaRepository<CProject,Long> {

}
