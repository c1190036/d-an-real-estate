package devcamp.thongnh.home24h.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "photo")
public class CPhoto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
     
    @Lob
    @Column(name = "content")
    private byte[] content;

    @ManyToOne
    @JoinColumn(name = "_real_estate_id")
    @JsonIgnore
    private CRealEstate realestate;

    public CPhoto() {
    }

    public CPhoto(Long id, byte[] content, CRealEstate realestate) {
        this.id = id;
        this.content = content;
        this.realestate = realestate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public CRealEstate getRealEstate() {
        return realestate;
    }

    public void setRealEstate(CRealEstate realestate) {
        this.realestate = realestate;
    }

}
