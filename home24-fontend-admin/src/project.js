'use strict';
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gURL = "http://localhost:8080/project";
var gProjectId = 0;
var gProvinceId = 0;
var gDistrictId = 0;
var gWardId = 0;
var gStreetId = 0;
let projectTable = $("#project-table").DataTable({
  columns: [
    { data: "id" },
    { data: "name" },
    {
      data: "district",
      render: function (data, type, row) {
        if (data && data.province && data.province.name) {
          return data.province.name; // Trả về tên tỉnh thành
        } else {
          return ""; // Không hiển thị gì nếu dữ liệu là null
        }
      }
    },
    {
      data: "district",
      render: function (data, type, row) {
        if (data && data.name) {
          return data.name; // Trả về tên district
        } else {
          return ""; // Không hiển thị gì nếu dữ liệu là null
        }
      }
    },
    {
      data: "ward",
      render: function (data, type, row) {
        if (data && data.prefix && data.name) {
          return data.prefix + " " + data.name; // Trả về prefix+name của ward
        } else {
          return ""; // Không hiển thị gì nếu dữ liệu là null
        }
      }
    },
    {
      data: "street",
      render: function (data, type, row) {
        if (data && data.prefix && data.name) {
          return data.prefix + " " + data.name; // Trả về prefix+name của ward
        } else {
          return ""; // Không hiển thị gì nếu dữ liệu là null
        }
      }
    },
    { data: 'address' },
    { data: 'slogan' },
    { data: 'acreage' },
    { data: 'constructArea' },
    { data: 'numBlock' },
    { data: 'numFloors' },
    { data: 'numApartment' },
    { data: 'apartmentArea' },
    { data: 'investor' },
    { data: 'constructionContractor' },
    { data: 'designUnit' },
    { data: 'utilities' },
    { data: 'regionLink' },
    { data: 'photo' },
    { data: 'lat' },
    { data: 'lng' },
    { data: 'action' },
  ],
  columnDefs: [
    {
      targets: -1,
      defaultContent: `<i class="fas fa-edit text-primary"></i>
      | <i class="fas fa-trash text-danger"></i>`,
    },
  ],
})

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$("#project-table").on("click", ".fa-edit", onUpdateProjectClick);
$("#project-table").on("click", ".fa-trash", onDeleteProjectClick);
$("#create-project").on("click", onPostProjectClick);
$("#update-project").on("click", onPutProjectClick);
$("#delete-project").on("click", onConfirmDeleteProjectClick);
$("#delete-all-project").on("click", onDeleteAllProjectClick);
$("#select-province").on("change", onChangeProvinceClick);
$("#select-district").on("change", onChangeDistrictClick);
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// hàm xử lý sự kiện tải trang
$(document).ready(function () {
  onLoadPage();
})
function onLoadPage() {
  gProjectId = 0;
  gProvinceId = 0;
  gDistrictId = 0;
  gWardId = 0;
  gStreetId = 0;
  $(".project-class input").val("");
  $("#select-province").empty();
  $("#select-district").empty();
  $("#select-ward").empty();
  $("#select-street").empty();
  const optionProvince = $("<option>").val(0).text("Chọn Tỉnh(Thành Phố)").appendTo("#select-province");
  const optionDistrict = $("<option>").val(0).text("Chọn Quận(Huyện)").appendTo("#select-district");
  const optionWard = $("<option>").val(0).text("Chọn Phường").appendTo("#select-ward");
  const optionStreet = $("<option>").val(0).text("Chọn Đường").appendTo("#select-street");
  $.get(gURL, loadProjectToTable);
  $.get("http://localhost:8080/province", loadProvinceToSelect);
  
}
function onUpdateProjectClick() {
  let vSelectRow = $(this).parents('tr');
  let vSelectedData = projectTable.row(vSelectRow).data();
  gProjectId = vSelectedData.id;
  $.get(gURL + "/" + gProjectId, loadProjectToInput);
}
function onDeleteProjectClick() {
  let vSelectRow = $(this).parents('tr');
  let vSelectedData = projectTable.row(vSelectRow).data();
  gProjectId = vSelectedData.id;
  $("#modal-delete-project").modal('show');
}
function onPostProjectClick() {
  var vProject = {
    name: "",
    address: "",
    slogan: "",
    description: "",
    acreage: "",
    constructArea: "",
    numBlock: "",
    numFloors: "",
    numApartment: "",
    apartmentArea: "",
    investor: "",
    constructionContractor: "",
    designUnit: "",
    utilities: "",
    regionLink: "",
    photo: "",
    lat: "",
    lng: "",
  }
  getDataInput(vProject);
  console.log(vProject);
  var vResult = validateProject(vProject);
  console.log(JSON.stringify(vProject));
  if (vResult) {
    $.ajax({
      url: gURL + "?districtId=" + gDistrictId + "&wardId=" + gWardId + "&streetId=" + gStreetId,
      method: 'POST',
      data: JSON.stringify(vProject),
      contentType: 'application/json',
      success: () => {
        alert('Project created successfully');
        onLoadPage();
      },
      error: (err) => alert(err.responseText),
    });
  }
}
function onPutProjectClick() {
  var vProject = {
    name: "",
    address: "",
    slogan: "",
    description: "",
    acreage: "",
    constructArea: "",
    numBlock: "",
    numFloors: "",
    numApartment: "",
    apartmentArea: "",
    investor: "",
    constructionContractor: "",
    designUnit: "",
    utilities: "",
    regionLink: "",
    photo: "",
    lat: "",
    lng: "",
  }
  getDataInput(vProject);
  var vResult = validateProject(vProject);
  if (vResult) {
    $.ajax({
      url: gURL + "/" + gProjectId + "?districtId=" + gDistrictId + "&wardId=" + gWardId + "&streetId=" + gStreetId,
      method: 'PUT',
      data: JSON.stringify(vProject),
      contentType: 'application/json',
      success: () => {
        alert('Project update successfully');
        onLoadPage();
      },
      error: (err) => alert(err.responseText),
    });
  }
}
function onDeleteAllProjectClick() {
  gProjectId = 0;
  $("#modal-delete-project").modal('show');
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function loadProjectToTable(paramProject) {
  projectTable.clear().rows.add(paramProject).draw();
}

/*<Load to select> */
function loadProvinceToSelect(paramProvince) {
  $.each(paramProvince, function (index, province) {
    const option = $("<option>")
      .val(province.id)
      .text(province.name);
    $("#select-province").append(option);
  });
}
function onChangeProvinceClick() {
  gProvinceId = $(this).val();
  $.get("http://localhost:8080/province/" + gProvinceId + "/district", loadDistrictToSelect);
}
function onChangeDistrictClick() {
  gDistrictId = $(this).val();
  $.get("http://localhost:8080/province/district/" + gDistrictId + "/ward", loadWardToSelect);
  $.get("http://localhost:8080/province/district/" + gDistrictId + "/street", loadStreetToSelect);
}
function loadDistrictToSelect(paramDistrict) {
  $("#select-district").empty();
  $.each(paramDistrict, function (index, district) {
    const option = $("<option>")
      .val(district.id)
      .text(district.prefix + " " + district.name);
    $("#select-district").append(option);
  });
}
function loadWardToSelect(parramWard) {
  $("#select-ward").empty();
  $.each(parramWard, function (index, ward) {
    const option = $("<option>")
      .val(ward.id)
      .text(ward.prefix + " " + ward.name);
    $("#select-ward").append(option);
  });
}
function loadStreetToSelect(parramStreet) {
  $("#select-street").empty();
  $.each(parramStreet, function (index, street) {
    const option = $("<option>")
      .val(street.id)
      .text(street.prefix + " " + street.name);
    $("#select-street").append(option);
  });
}
/*</Load to select> */
function loadProjectToInput(paramProject) {
  console.log(paramProject);
  gProjectId = paramProject.id;
  $("#input-project-name").val(paramProject.name);
  $("#select-province").val(paramProject.provinceId);

  // Load district data
  $.get("http://localhost:8080/province/" + paramProject.provinceId + "/district", function (districtData) {
    loadDistrictToSelect(districtData);

    if (paramProject.district) {
      $("#select-district").val(paramProject.district.id);

      // Load ward data
      $.get("http://localhost:8080/province/district/" + paramProject.district.id + "/ward", function (wardData) {
        loadWardToSelect(wardData);

        if (paramProject.ward) {
          $("#select-ward").val(paramProject.ward.id);

          // Load street data
          $.get("http://localhost:8080/province/district/" + paramProject.district.id + "/street", function (streetData) {
            loadStreetToSelect(streetData);

            if (paramProject.street) {
              $("#select-street").val(paramProject.street.id);

              // Continue with setting other values
              $("#input-project-address").val(paramProject.address);
              $("#input-project-slogan").val(paramProject.slogan);
              $("#input-project-description").val(paramProject.description);
              $("#input-project-acreage").val(paramProject.acreage);
              $("#input-project-construct-area").val(paramProject.constructArea);
              $("#nput-project-num-block").val(paramProject.numBlock);
              $("#input-project-num-floors").val(paramProject.numFloors);
              $("#input-project-num-apartment").val(paramProject.numApartment);
              $("#input-project-apartment-area").val(paramProject.apartmentArea);
              $("#input-project-investor").val(paramProject.investor);
              $("#input-project-construction-contractor").val(paramProject.constructionContractor);
              $("#input-project-design-unit").val(paramProject.designUnit);
              $("#input-project-utilities").val(paramProject.utilities);
              $("#input-project-region-link").val(paramProject.regionLink);
              $("#input-project-photo").val(paramProject.photo);
              $("#input-project-lat").val(paramProject.lat);
              $("#input-project-lng").val(paramProject.lng);
              // ...
            }
          });
        } else {
          // Continue with setting other values
          $("#input-project-address").val(paramProject.address);
          $("#input-project-slogan").val(paramProject.slogan);
          $("#input-project-description").val(paramProject.description);
          $("#input-project-acreage").val(paramProject.acreage);
          $("#input-project-construct-area").val(paramProject.constructArea);
          $("#nput-project-num-block").val(paramProject.numBlock);
          $("#input-project-num-floors").val(paramProject.numFloors);
          $("#input-project-num-apartment").val(paramProject.numApartment);
          $("#input-project-apartment-area").val(paramProject.apartmentArea);
          $("#input-project-investor").val(paramProject.investor);
          $("#input-project-construction-contractor").val(paramProject.constructionContractor);
          $("#input-project-design-unit").val(paramProject.designUnit);
          $("#input-project-utilities").val(paramProject.utilities);
          $("#input-project-region-link").val(paramProject.regionLink);
          $("#input-project-photo").val(paramProject.photo);
          $("#input-project-lat").val(paramProject.lat);
          $("#input-project-lng").val(paramProject.lng);
          // ...
        }
      });
    } else {
      // Continue with setting other values
      $("#input-project-address").val(paramProject.address);
      $("#input-project-slogan").val(paramProject.slogan);
      $("#input-project-description").val(paramProject.description);
      $("#input-project-acreage").val(paramProject.acreage);
      $("#input-project-construct-area").val(paramProject.constructArea);
      $("#nput-project-num-block").val(paramProject.numBlock);
      $("#input-project-num-floors").val(paramProject.numFloors);
      $("#input-project-num-apartment").val(paramProject.numApartment);
      $("#input-project-apartment-area").val(paramProject.apartmentArea);
      $("#input-project-investor").val(paramProject.investor);
      $("#input-project-construction-contractor").val(paramProject.constructionContractor);
      $("#input-project-design-unit").val(paramProject.designUnit);
      $("#input-project-utilities").val(paramProject.utilities);
      $("#input-project-region-link").val(paramProject.regionLink);
      $("#input-project-photo").val(paramProject.photo);
      $("#input-project-lat").val(paramProject.lat);
      $("#input-project-lng").val(paramProject.lng);
      // ...
    }
  });
}
function getDataInput(paramProject) {
  gDistrictId = $("#select-district").val();
  gWardId = $("#select-ward").val();
  gStreetId = $("#select-street").val();
  paramProject.name = $("#input-project-name").val();
  paramProject.address = $("#input-project-address").val();
  paramProject.slogan = $("#input-project-slogan").val();
  paramProject.notdescriptione = $("#input-project-description").val();
  paramProject.acreage = $("#input-project-acreage").val();
  paramProject.constructArea = $("#input-project-construct-area").val();
  paramProject.numBlock = $("#input-project-num-block").val();
  paramProject.numFloors = $("#input-project-num-floors").val();
  paramProject.numApartment = $("#input-project-num-apartment").val();
  paramProject.apartmentArea = $("#input-project-apartment-area").val();
  paramProject.investor = $("#input-project-investor").val();
  paramProject.constructionContractor = $("#input-project-construction-contractor").val();
  paramProject.designUnit = $("#input-project-design-unit").val();
  paramProject.utilities = $("#input-project-utilities").val();
  paramProject.regionLink = $("#input-project-region-link").val();
  paramProject.photo = $("#input-project-photo").val();
  paramProject.lat = $("#input-project-lat").val();
  paramProject.lng = $("#input-project-lng").val();
}
function validateProject(paramProject) {
  if (paramProject.lat !== "" && !$.isNumeric(paramProject.lat)) {
    alert("Lat phải là số");
    return false;
  } else if (paramProject.lng !== "" && !$.isNumeric(paramProject.lng)) {
    alert("Lng phải là số");
    return false;
  } else if (gDistrictId == 0) {
    alert("Phải Chọn Quận(Huyện) trước khi save");
    return false;
  } else if (paramProject.name === "") {
    alert("Không được để trống Project Name");
    return false;
  } else {
    return true;
  }
}
function onConfirmDeleteProjectClick() {
  if (gProjectId != 0) {
    $.ajax({
      url: gURL + "/" + gProjectId,
      method: "DELETE",
      success: () => {
        alert("Project id: " + gProjectId + " successfully delete");
        $('#modal-delete-project').modal('hide');
        onLoadPage();
      }
    })
  }
  else {
    $.ajax({
      url: gURL,
      method: "DELETE",
      success: () => {
        alert(" All Project successfully delete");
        $('#modal-delete-project').modal('hide');
        onLoadPage();
      }
    })
  }
}


