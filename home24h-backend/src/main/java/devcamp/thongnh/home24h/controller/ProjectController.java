package devcamp.thongnh.home24h.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import devcamp.thongnh.home24h.model.CDistrict;
import devcamp.thongnh.home24h.model.CProject;
import devcamp.thongnh.home24h.model.CProvince;
import devcamp.thongnh.home24h.model.CStreet;
import devcamp.thongnh.home24h.model.CWard;
import devcamp.thongnh.home24h.respository.IDistrictRespository;
import devcamp.thongnh.home24h.respository.IProjectRespository;
import devcamp.thongnh.home24h.respository.IProvinceRespository;
import devcamp.thongnh.home24h.respository.IStreetRespository;
import devcamp.thongnh.home24h.respository.IWardRespository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class ProjectController {
    @Autowired
    IProvinceRespository pIProvinceRespository;

    @Autowired
    IDistrictRespository pDistrictRespository;

    @Autowired
    IWardRespository pIWardRespository;

    @Autowired
    IStreetRespository pIStreetRespository;

    @Autowired
    IProjectRespository pIProjectRespository;

    // Get All Project
    @GetMapping("/project")
    public ResponseEntity<List<CProject>> getAllProject() {
        try {
            List<CProject> projectList = pIProjectRespository.findAll();
            return new ResponseEntity<>(projectList, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Project By Id
    @GetMapping("/project/{id}")
    public ResponseEntity<CProject> getProjectById(@PathVariable("id") long pId) {
        try {
            Optional<CProject> projectId = pIProjectRespository.findById(pId);
            if (projectId.isPresent()) {
                return new ResponseEntity<>(projectId.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Project By Id Of District
    @GetMapping("district/{id}/project")
    public ResponseEntity<List<CProject>> getProjectByIdOfDistrict(@PathVariable("id") long pId) {
        try {
            Optional<CDistrict> districtId = pDistrictRespository.findById(pId);
            if (districtId.isPresent()) {
                return new ResponseEntity<>(districtId.get().getProject(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Project By Id Of Province
    @GetMapping("/province/{id}/project")
    public ResponseEntity<List<CProject>> getProjectByIdOfProvince(@PathVariable("id") long pId) {
        try {
            Optional<CProvince> provinceId = pIProvinceRespository.findById(pId);
            if (provinceId.isPresent()) {
                List<CDistrict> districtList = provinceId.get().getDistrict();
                List<CProject> projectsList = new ArrayList<>();
                districtList.forEach(district -> projectsList.addAll(district.getProject()));
                return new ResponseEntity<>(projectsList, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    // Post Project
    @PostMapping("/project")
    public ResponseEntity<Object> postProject(@RequestParam("districtId") long pDistrictId,
            @RequestBody CProject pProject, @RequestParam("wardId") Long pWardId,
            @RequestParam("streetId") Long pStreetId) {
        try {
            Optional<CDistrict> districtId = pDistrictRespository.findById(pDistrictId);
            Optional<CWard> wardId = pIWardRespository.findById(pWardId);
            Optional<CStreet> streetId = pIStreetRespository.findById(pStreetId);
            if (!districtId.isPresent()) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                pProject.setProvinceId(districtId.get().getProvince().getId());
                pProject.setDistrict(districtId.get());
                if (wardId.isPresent()) {
                    pProject.setWard(wardId.get());
                }
                if (streetId.isPresent()) {
                    pProject.setStreet(streetId.get());
                }
                pIProjectRespository.save(pProject);
                return new ResponseEntity<>(HttpStatus.CREATED);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Put Project
    @PutMapping("/project/{projectId}")
    public ResponseEntity<Object> putProject(@RequestParam("districtId") long pDistrictId,
            @PathVariable("projectId") long pProjectId, @RequestParam("wardId") Long pWardId,
            @RequestParam("streetId") Long pStreetId,
            @RequestBody CProject pProject) {
        try {
            Optional<CDistrict> districtId = pDistrictRespository.findById(pDistrictId);
            Optional<CProject> projectId = pIProjectRespository.findById(pProjectId);
            Optional<CWard> wardId = pIWardRespository.findById(pWardId);
            Optional<CStreet> streetId = pIStreetRespository.findById(pStreetId);
            if (!districtId.isPresent() || !projectId.isPresent()) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                pProject.setId(pProjectId); 
                pProject.setProvinceId(districtId.get().getProvince().getId());
                pProject.setDistrict(districtId.get());
                if (wardId.isPresent()) {
                    pProject.setWard(wardId.get());
                }
                if (streetId.isPresent()) {
                    pProject.setStreet(streetId.get());
                }
                pIProjectRespository.save(pProject);
                return new ResponseEntity<>(HttpStatus.OK);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete Project
    @DeleteMapping("/project/{projectId}")
    public ResponseEntity<Object> deleteProjectById(@PathVariable("projectId") long pProjectId) {
        try {
            Optional<CProject> projectId = pIProjectRespository.findById(pProjectId);
            if (projectId.isPresent()) {
                pIProjectRespository.deleteById(pProjectId);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete All Project
    @DeleteMapping("/project")
    public ResponseEntity<Object> deleteAllProject() {
        try {
            pIProjectRespository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
