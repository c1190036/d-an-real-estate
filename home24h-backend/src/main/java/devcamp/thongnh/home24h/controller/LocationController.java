package devcamp.thongnh.home24h.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import devcamp.thongnh.home24h.model.CLocation;
import devcamp.thongnh.home24h.respository.ILocationResponsetory;

@RequestMapping("/")
@RestController
@CrossOrigin
public class LocationController {
    @Autowired
    ILocationResponsetory pILocationResponsetory;

    // Get All Location
    @GetMapping("/location")
    public ResponseEntity<List<CLocation>> getAllLocation() {
        try {
            List<CLocation> locationList = pILocationResponsetory.findAll();
            return new ResponseEntity<>(locationList, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Location By Id
    @GetMapping("/location/{id}")
    public ResponseEntity<CLocation> getLocationById(@PathVariable("id") long pId) {
        try {
            Optional<CLocation> locationId = pILocationResponsetory.findById(pId);
            if (locationId.isPresent()) {
                return new ResponseEntity<>(locationId.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Post Location
    @PostMapping("/location")
    public ResponseEntity<Object> postLocation(@RequestBody CLocation pLocation) {
        try {
            pILocationResponsetory.save(pLocation);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Put Location By Id
    @PutMapping("/location/{id}")
    public ResponseEntity<Object> putLocationById(@PathVariable("id") long pId, @RequestBody CLocation pLocation) {
        try {
            Optional<CLocation> locationId = pILocationResponsetory.findById(pId);
            if (locationId.isPresent()) {
                CLocation locationUpdate = locationId.get();
                pILocationResponsetory.save(locationUpdate);
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete Location By Id
    @DeleteMapping("/location/{id}")
    public ResponseEntity<Object> deleteLocationById(@PathVariable("id") long pId) {
        try {
            Optional<CLocation> locationId = pILocationResponsetory.findById(pId);
            if (locationId.isPresent()) {
                pILocationResponsetory.deleteById(pId);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete All Location
    @DeleteMapping("/location")
    public ResponseEntity<Object> deleteAllLocation() {
        try {
            pILocationResponsetory.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
