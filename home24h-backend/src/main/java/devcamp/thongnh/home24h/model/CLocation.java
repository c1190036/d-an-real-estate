package devcamp.thongnh.home24h.model;
import javax.persistence.*;

@Entity
@Table(name = "locations")
public class CLocation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "Latitude", nullable = false)
    private float latitude;

    @Column(name = "Longitude", nullable = false)
    private float longitude;

    // Constructors, getters, and setters
    // ...

    // Constructors
    public CLocation() {
    }

    public CLocation(float latitude, float longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    // Getters and Setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }
}
