package devcamp.thongnh.home24h.respository;

import java.util.Optional;

import org.hibernate.mapping.List;
import org.springframework.data.jpa.repository.JpaRepository;

import devcamp.thongnh.home24h.model.CCustomers;

public interface ICustomerRespository extends JpaRepository<CCustomers,Long> {
    // Kiểm tra xem mobile đã tồn tại trong cơ sở dữ liệu hay chưa
    Optional<CCustomers> findByMobile(String mobile);

    // Kiểm tra xem email đã tồn tại trong cơ sở dữ liệu hay chưa
    Optional<CCustomers> findByEmail(String email);

    //Kiểm tra username đã tồn tại trong cơ sở dữ liệu hay chưa
    Optional<CCustomers> findByUsername(String username);
    
}
