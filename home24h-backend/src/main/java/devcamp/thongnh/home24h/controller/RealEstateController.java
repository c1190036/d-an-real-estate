package devcamp.thongnh.home24h.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import devcamp.thongnh.home24h.model.CCustomers;
import devcamp.thongnh.home24h.model.CProject;
import devcamp.thongnh.home24h.model.CRealEstate;
import devcamp.thongnh.home24h.model.CStreet;
import devcamp.thongnh.home24h.model.CWard;
import devcamp.thongnh.home24h.respository.ICustomerRespository;
import devcamp.thongnh.home24h.respository.IProjectRespository;
import devcamp.thongnh.home24h.respository.IRealEstateResponsitory;
import devcamp.thongnh.home24h.respository.IStreetRespository;
import devcamp.thongnh.home24h.respository.IWardRespository;

@RequestMapping("/")
@RestController
@CrossOrigin
public class RealEstateController {
    @Autowired
    IProjectRespository pIProjectRespository;

    @Autowired
    IWardRespository pIWardRespository;

    @Autowired
    ICustomerRespository pICustomerRespository;

    @Autowired
    IStreetRespository pIStreetRespository;

    @Autowired
    IRealEstateResponsitory pIRealEstateResponsitory;

    // Get All RealEstate
    @GetMapping("/realestate")
    public ResponseEntity<List<CRealEstate>> getAllRealEstate() {
        try {
            List<CRealEstate> realEstateList = pIRealEstateResponsitory.findAll();
            return new ResponseEntity<>(realEstateList, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get RealEstate By Id
    @GetMapping("/realestate/{id}")
    public ResponseEntity<CRealEstate> getRealEstateById(@PathVariable("id") long pId) {
        try {
            Optional<CRealEstate> realEstateId = pIRealEstateResponsitory.findById(pId);
            if (realEstateId.isPresent()) {
                return new ResponseEntity<>(realEstateId.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get RealEstate By Id Of Ward
    @GetMapping("/province/district/ward/{id}/realestate")
    public ResponseEntity<List<CRealEstate>> getAllRealEstateByIdOfWard(@PathVariable("id") long pId) {
        try {
            Optional<CWard> wardId = pIWardRespository.findById(pId);
            if (wardId.isPresent()) {
                return new ResponseEntity<>(wardId.get().getRealEstate(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Post RealEstate
    @PostMapping("/realestate")
    public ResponseEntity<Object> postRealEstate(@RequestBody CRealEstate pCRealEstate,
            @RequestParam("customerId") long pCustomerId, @RequestParam("wardId") long pWardId,
            @RequestParam("streetId") long pStreetId, @RequestParam("projectId") long pProjectId) {
        try {
            Optional<CCustomers> customerId = pICustomerRespository.findById(pCustomerId);
            Optional<CWard> wardId = pIWardRespository.findById(pWardId);
            Optional<CProject> projectId = pIProjectRespository.findById(pProjectId);
            if (customerId.isPresent()) {
                pCRealEstate.setCustomers(customerId.get());
            } else if (!customerId.isPresent()) {
                return new ResponseEntity<>("Không tìm thấy khách hàng", HttpStatus.NOT_FOUND);
            }
            if (!projectId.isPresent() && !wardId.isPresent()) {
                return new ResponseEntity<>("Địa chỉ không hợp lệ", HttpStatus.NOT_FOUND);
            }
            if (projectId.isPresent()) {
                pCRealEstate.setProvinceId(projectId.get().getProvinceId());
                pCRealEstate.setDistrictId(projectId.get().getDistrict().getId());
                pCRealEstate.setWard(projectId.get().getWard());
                CStreet streetId = projectId.get().getStreet();
                if (streetId != null) {
                    pCRealEstate.setStreetId(projectId.get().getStreet().getId());
                } else {
                    pCRealEstate.setStreetId(null);
                }
                pCRealEstate.setProject(projectId.get());
                pCRealEstate.setDateCreate(new Date());
            } else if (!projectId.isPresent()) {
                pCRealEstate.setProvinceId(wardId.get().getProvince());
                pCRealEstate.setDistrictId(wardId.get().getDistrict().getId());
                pCRealEstate.setWard(wardId.get());
                pCRealEstate.setStreetId(pStreetId);
                pCRealEstate.setProject(null);
                pCRealEstate.setDateCreate(new Date());
            }
            pIRealEstateResponsitory.save(pCRealEstate);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Put RealEstate
    @PutMapping("/realestate/{id}")
    public ResponseEntity<Object> putRealEstate(@RequestBody CRealEstate pCRealEstate, @PathVariable("id") long pId,
            @RequestParam("customerId") long pCustomerId, @RequestParam("wardId") long pWardId,
            @RequestParam("streetId") long pStreetId, @RequestParam("projectId") long pProjectId) {
        try {
            Optional<CRealEstate> realEstateId = pIRealEstateResponsitory.findById(pId);
            Optional<CCustomers> customerId = pICustomerRespository.findById(pCustomerId);
            Optional<CWard> wardId = pIWardRespository.findById(pWardId);
            Optional<CProject> projectId = pIProjectRespository.findById(pProjectId);

            if (realEstateId.isPresent()) {
                pCRealEstate.setId(pId);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            if (customerId.isPresent()) {
                pCRealEstate.setCustomers(customerId.get());
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            if (!projectId.isPresent() && !wardId.isPresent()) {
                return new ResponseEntity<>("Địa chỉ không hợp lệ", HttpStatus.NOT_FOUND);
            }
            if (projectId.isPresent()) {
                pCRealEstate.setProvinceId(projectId.get().getProvinceId());
                pCRealEstate.setDistrictId(projectId.get().getDistrict().getId());
                pCRealEstate.setWard(projectId.get().getWard());
                CStreet streetId = projectId.get().getStreet();
                if (streetId != null) {
                    pCRealEstate.setStreetId(projectId.get().getStreet().getId());
                } else {
                    pCRealEstate.setStreetId(null);
                }
                pCRealEstate.setProject(projectId.get());
                pCRealEstate.setDateCreate(new Date());
            } else if (!projectId.isPresent()) {
                pCRealEstate.setProvinceId(wardId.get().getProvince());
                pCRealEstate.setDistrictId(wardId.get().getDistrict().getId());
                pCRealEstate.setWard(wardId.get());
                pCRealEstate.setStreetId(pStreetId);
                pCRealEstate.setProject(null);
                pCRealEstate.setDateCreate(new Date());
            }
            pIRealEstateResponsitory.save(pCRealEstate);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete RealEstate
    @DeleteMapping("/realestate/{id}")
    public ResponseEntity<Object> deleteRealEstate(@PathVariable("id") long pId) {
        try {
            Optional<CRealEstate> realEstateId = pIRealEstateResponsitory.findById(pId);

            if (realEstateId.isPresent()) {
                pIRealEstateResponsitory.deleteById(pId);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete All RealEstate
    @DeleteMapping("/realestate")
    public ResponseEntity<Object> deleteAllRealEstate() {
        try {
            pIRealEstateResponsitory.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
