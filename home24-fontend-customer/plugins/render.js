// render.js
// Các hàm render content cho các trang khác nhau

function renderHomePage() {
    document.getElementById("content").innerHTML = "<h2>Home Page</h2><p>Welcome to our website!</p>";
}

function renderAboutPage() {
    document.getElementById("content").innerHTML = "<h2>About Us</h2><p>Learn more about our company.</p>";
}

function renderServicesPage() {
    document.getElementById("content").innerHTML = "<h2>Our Services</h2><p>We offer a variety of services.</p>";
}

function renderContactPage() {
    document.getElementById("content").innerHTML = "<h2>Contact Us</h2><p>Get in touch with us.</p>";
}