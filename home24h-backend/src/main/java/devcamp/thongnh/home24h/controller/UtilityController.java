package devcamp.thongnh.home24h.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import devcamp.thongnh.home24h.model.CUtility;
import devcamp.thongnh.home24h.respository.IUtilityResponsitory;

@RequestMapping("/")
@RestController
@CrossOrigin
public class UtilityController {
    @Autowired
    IUtilityResponsitory pIUtilityResponsitory;

    // Get All Utility
    @GetMapping("/utility")
    public ResponseEntity<List<CUtility>> getAllUtility() {
        try {
            List<CUtility> utilityList = pIUtilityResponsitory.findAll();
            return new ResponseEntity<>(utilityList, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Utility By Id
    @GetMapping("/utility/{id}")
    public ResponseEntity<CUtility> getUtilityById(@PathVariable("id") long pId) {
        try {
            Optional<CUtility> utilityId = pIUtilityResponsitory.findById(pId);
            if (utilityId.isPresent()) {
                return new ResponseEntity<>(utilityId.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Post Utility
    @PostMapping("/utility")
    public ResponseEntity<Object> postUtility(@RequestBody CUtility pUtility) {
        try {
            pIUtilityResponsitory.save(pUtility);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Put Utility By Id
    @PutMapping("/utility/{id}")
    public ResponseEntity<Object> putUtilityById(@PathVariable("id") long pId, @RequestBody CUtility pUtility) {
        try {
            Optional<CUtility> utilityId = pIUtilityResponsitory.findById(pId);
            if (utilityId.isPresent()) {
                CUtility utilityUpdate = utilityId.get();
                pIUtilityResponsitory.save(utilityUpdate);
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete Utility By Id
    @DeleteMapping("/utility/{id}")
    public ResponseEntity<Object> deleteUtilityById(@PathVariable("id") long pId) {
        try {
            Optional<CUtility> utilityId = pIUtilityResponsitory.findById(pId);
            if (utilityId.isPresent()) {
                pIUtilityResponsitory.deleteById(pId);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete All Utility
    @DeleteMapping("/utility")
    public ResponseEntity<Object> deleteAllUtility() {
        try {
            pIUtilityResponsitory.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
