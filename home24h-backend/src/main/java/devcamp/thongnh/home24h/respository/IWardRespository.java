package devcamp.thongnh.home24h.respository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import devcamp.thongnh.home24h.model.CWard;

public interface IWardRespository extends JpaRepository<CWard,Long> {
    Optional<CWard> findByProvince(Long province);
}
