package devcamp.thongnh.home24h.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import devcamp.thongnh.home24h.model.CSubscriptions;
import devcamp.thongnh.home24h.respository.ISubscriptionResponsitory;

@RequestMapping("/")
@RestController
@CrossOrigin
public class SubscriptionsController {
    @Autowired
    ISubscriptionResponsitory pISubscriptionsResponsitory;

    // Get All Subscriptions
    @GetMapping("/subscriptions")
    public ResponseEntity<List<CSubscriptions>> getAllSubscriptions() {
        try {
            List<CSubscriptions> subscriptionsList = pISubscriptionsResponsitory.findAll();
            return new ResponseEntity<>(subscriptionsList, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Subscriptions By Id
    @GetMapping("/subscriptions/{id}")
    public ResponseEntity<CSubscriptions> getSubscriptionsById(@PathVariable("id") long pId) {
        try {
            Optional<CSubscriptions> subscriptionsId = pISubscriptionsResponsitory.findById(pId);
            if (subscriptionsId.isPresent()) {
                return new ResponseEntity<>(subscriptionsId.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Post Subscriptions
    @PostMapping("/subscriptions")
    public ResponseEntity<Object> postSubscriptions(@RequestBody CSubscriptions pSubscriptions) {
        try {
            pISubscriptionsResponsitory.save(pSubscriptions);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Put Subscriptions By Id
    @PutMapping("/subscriptions/{id}")
    public ResponseEntity<Object> putSubscriptionsById(@PathVariable("id") long pId, @RequestBody CSubscriptions pSubscriptions) {
        try {
            Optional<CSubscriptions> subscriptionsId = pISubscriptionsResponsitory.findById(pId);
            if (subscriptionsId.isPresent()) {
                CSubscriptions subscriptionsUpdate = subscriptionsId.get();
                pISubscriptionsResponsitory.save(subscriptionsUpdate);
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete Subscriptions By Id
    @DeleteMapping("/subscriptions/{id}")
    public ResponseEntity<Object> deleteSubscriptionsById(@PathVariable("id") long pId) {
        try {
            Optional<CSubscriptions> subscriptionsId = pISubscriptionsResponsitory.findById(pId);
            if (subscriptionsId.isPresent()) {
                pISubscriptionsResponsitory.deleteById(pId);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete All Subscriptions
    @DeleteMapping("/subscriptions")
    public ResponseEntity<Object> deleteAllSubscriptions() {
        try {
            pISubscriptionsResponsitory.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
