package devcamp.thongnh.home24h.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import devcamp.thongnh.home24h.model.CEmployees;
import devcamp.thongnh.home24h.respository.IEmployeesRespository;

@RequestMapping("/")
@RestController
@CrossOrigin
public class EmployeesController {
    @Autowired
    IEmployeesRespository pEmployeesRespository;

    // Get All Employees
    @GetMapping("/employees")
    public ResponseEntity<List<CEmployees>> getAllEmployees() {
        try {
            List<CEmployees> employeesList = pEmployeesRespository.findAll();
            return new ResponseEntity<>(employeesList, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Employees By Id
    @GetMapping("/employees/{id}")
    public ResponseEntity<CEmployees> getEmployeesById(@PathVariable("id") long pId) {
        try {
            Optional<CEmployees> employeesId = pEmployeesRespository.findById(pId);
            if (employeesId.isPresent()) {
                return new ResponseEntity<>(employeesId.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Post Emloyees
    @PostMapping("/employees")
    public ResponseEntity<Object> postEmployees(@RequestBody CEmployees pEmployees) {
        try {
            Optional<CEmployees> employeesUsername = pEmployeesRespository.findByUsername(pEmployees.getUsername());
            Optional<CEmployees> employeesEmail = pEmployeesRespository.findByEmail(pEmployees.getEmail());
            if (employeesUsername.isPresent() || employeesEmail.isPresent()) {
                return new ResponseEntity<>("Username or Email already exists", HttpStatus.CONFLICT);
            } else {
                CEmployees savedEmployee = pEmployeesRespository.save(pEmployees);
                return new ResponseEntity<>(savedEmployee, HttpStatus.CREATED);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Put Emloyees
    @PutMapping("/employees/{id}")
    public ResponseEntity<Object> putEmloyeesById(@RequestBody CEmployees pEmployees, @PathVariable("id") long pId) {
        try {
            Optional<CEmployees> employeesId = pEmployeesRespository.findById(pId);
            Optional<CEmployees> employeesUsername = pEmployeesRespository.findByUsername(pEmployees.getUsername());
            Optional<CEmployees> employeesEmail = pEmployeesRespository.findByEmail(pEmployees.getEmail());
            if (!employeesId.isPresent()) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else if (employeesUsername.isPresent() && employeesUsername.get().getId() != pId) {
                return new ResponseEntity<>("Username already exists", HttpStatus.CONFLICT);
            } else if (employeesEmail.isPresent() && employeesEmail.get().getId() != pId) {
                return new ResponseEntity<>("Email already exists", HttpStatus.CONFLICT);
            } else {
                CEmployees employeeUpdate = employeesId.get();
                // Cập nhật thông tin từ pEmployees vào employeeUpdate
                employeeUpdate.setLastName(pEmployees.getLastName());
                employeeUpdate.setFirstName(pEmployees.getFirstName());
                employeeUpdate.setTitle(pEmployees.getTitle());
                employeeUpdate.setTitleOfCourtesy(pEmployees.getTitleOfCourtesy());
                employeeUpdate.setBirthDate(pEmployees.getBirthDate());
                employeeUpdate.setHireDate(pEmployees.getHireDate());
                employeeUpdate.setAddress(pEmployees.getAddress());
                employeeUpdate.setCity(pEmployees.getCity());
                employeeUpdate.setRegion(pEmployees.getRegion());
                employeeUpdate.setPostalCode(pEmployees.getPostalCode());
                employeeUpdate.setCountry(pEmployees.getCountry());
                employeeUpdate.setHomePhone(pEmployees.getHomePhone());
                employeeUpdate.setExtension(pEmployees.getExtension());
                employeeUpdate.setPhoto(pEmployees.getPhoto());
                employeeUpdate.setNotes(pEmployees.getNotes());
                employeeUpdate.setReportsTo(pEmployees.getReportsTo());
                employeeUpdate.setUsername(pEmployees.getUsername());
                employeeUpdate.setPassword(pEmployees.getPassword());
                employeeUpdate.setEmail(pEmployees.getEmail());
                employeeUpdate.setActivated(pEmployees.getActivated());
                employeeUpdate.setProfile(pEmployees.getProfile());
                employeeUpdate.setUserLevel(pEmployees.getUserLevel());
                CEmployees employeeupdatedResult = pEmployeesRespository.save(employeeUpdate);
                return new ResponseEntity<>(employeeupdatedResult, HttpStatus.OK);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete Employees By Id
    @DeleteMapping("/employees/{id}")
    public ResponseEntity<Object> deleteEmployById(@PathVariable("id") long pId) {
        try {
            Optional<CEmployees> employeesId = pEmployeesRespository.findById(pId);
            if (employeesId.isPresent()) {
                pEmployeesRespository.deleteById(pId);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete All Employees
    @DeleteMapping("/employees")
    public ResponseEntity<Object> deleteAllEmploy() {
        try {
            pEmployeesRespository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
