
package devcamp.thongnh.home24h.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import devcamp.thongnh.home24h.model.CRealEstate;
import devcamp.thongnh.home24h.respository.IRealEstateResponsitory;

@Service
@Transactional
public class RealEstateService {

    private final IRealEstateResponsitory pIRealEstateResponsitory;

    @Autowired
    public RealEstateService(IRealEstateResponsitory pIRealEstateResponsitory) {
        this.pIRealEstateResponsitory = pIRealEstateResponsitory;
    }

    public CRealEstate saveImageFile(CRealEstate photo) {
        return pIRealEstateResponsitory.save(photo);
    }
}