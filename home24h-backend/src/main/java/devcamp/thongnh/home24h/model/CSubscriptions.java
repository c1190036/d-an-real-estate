package devcamp.thongnh.home24h.model;

import javax.persistence.*;

@Entity
@Table(name = "subscriptions")
public class CSubscriptions {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "user", length = 255)
    private String user;

    @Column(name = "endpoint", columnDefinition = "LONGTEXT", nullable = false)
    private String endpoint;

    @Column(name = "publickey", length = 255, nullable = false)
    private String publicKey;

    @Column(name = "authenticationtoken", length = 255, nullable = false)
    private String authenticationToken;

    @Column(name = "contentencoding", length = 255, nullable = false)
    private String contentEncoding;
    // Constructors, getters, and setters

    public CSubscriptions() {
    }

    public CSubscriptions(Long id, String user, String endpoint, String publicKey, String authenticationToken,
            String contentEncoding) {
        this.id = id;
        this.user = user;
        this.endpoint = endpoint;
        this.publicKey = publicKey;
        this.authenticationToken = authenticationToken;
        this.contentEncoding = contentEncoding;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public void setAuthenticationToken(String authenticationToken) {
        this.authenticationToken = authenticationToken;
    }

    public String getContentEncoding() {
        return contentEncoding;
    }

    public void setContentEncoding(String contentEncoding) {
        this.contentEncoding = contentEncoding;
    }

}
