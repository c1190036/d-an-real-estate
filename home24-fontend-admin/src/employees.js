'use strict';
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gURL = "http://localhost:8080/employees";
var gId = 0;
let employeesTable = $("#employees-table").DataTable({
  columns: [
    { data: "id" },
    { data: "lastName" },
    { data: "firstName" },
    { data: "title" },
    { data: "titleOfCourtesy" },
    { data: 'birthDate' },
    { data: 'hireDate' },
    { data: 'address' },
    { data: 'city' },
    { data: 'region' },
    { data: 'postalCode' },
    { data: 'country' },
    { data: 'homePhone' },
    { data: 'extension' },
    { data: 'photo' },
    { data: 'notes' },
    { data: 'reportsTo' },
    { data: 'username' },
    { data: 'password' },
    { data: 'email' },
    { data: 'activated' },
    { data: 'profile' },
    { data: 'userLevel' },
    { data: 'action' }
  ],
  columnDefs: [
    {
      targets: -1,
      defaultContent: `<i class="fas fa-edit text-primary"></i>
      | <i class="fas fa-trash text-danger"></i>`,
    },
  ],
})
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$("#employees-table").on("click", ".fa-edit", onUpdateEmployeesClick);
$("#employees-table").on("click", ".fa-trash", onDeleteEmployeesClick);
$("#create-employees").on("click", onPostEmployeesClick);
$("#update-employees").on("click", onPutEmployeesClick);
$("#delete-employees").on("click", onConfirmDeleteEmployeesClick);
$("#delete-all-employees").on("click", onDeleteAllEmployeesClick);
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// hàm xử lý sự kiện tải trang
$(document).ready(function () {
  onLoadPage();
})
function onLoadPage() {
  gId = 0;
  $("#input-employees").val("");
  $("#input-lat").val("");
  $("#input-lng").val("");
  $.get(gURL, loadEmployeesToTable);
}
function onUpdateEmployeesClick() {
  let vSelectRow = $(this).parents('tr');
  let vSelectedData = employeesTable.row(vSelectRow).data();
  gId = vSelectedData.id;
  $.get(gURL + "/" + gId, loadEmployeesToInput);
}
function onDeleteEmployeesClick() {
  let vSelectRow = $(this).parents('tr');
  let vSelectedData = employeesTable.row(vSelectRow).data();
  gId = vSelectedData.id;
  $("#modal-delete-employees").modal('show');
}
function onPostEmployeesClick() {
  var vEmployees = {
    contactName: "",
    contactTitle: "",
    address: "",
    mobile: "",
    email: "",
    note: "",
    createBy: "",
    updateBy: "",
  }
  getDataInput(vEmployees);
  console.log(vEmployees);
  var vResult = validateEmployees(vEmployees);
  console.log(JSON.stringify(vEmployees));
  if (vResult) {
    $.ajax({
      url: gURL,
      method: 'POST',
      data: JSON.stringify(vEmployees),
      contentType: 'application/json',
      success: () => {
        alert('Employees created successfully');
        onLoadPage();
      },
      error: (err) => alert(err.responseText),
    });
  }
}
function onPutEmployeesClick() {
  var vEmployees = {
    contactName: "",
    contactTitle: "",
    address: "",
    mobile: "",
    email: "",
    note: "",
    createBy: "",
    updateBy: "",
  }
  getDataInput(vEmployees);
  var vResult = validateEmployees(vEmployees);
  if (vResult) {
    $.ajax({
      url: gURL + "/" + gId,
      method: 'PUT',
      data: JSON.stringify(vEmployees),
      contentType: 'application/json',
      success: () => {
        alert('Employees update successfully');
        onLoadPage();
      },
      error: (err) => alert(err.responseText),
    });
  }
}
function onDeleteAllEmployeesClick() {
  gId = 0;
  $("#modal-delete-employees").modal('show');
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/0
function loadEmployeesToTable(paramEmployees) {
  employeesTable.clear().rows.add(paramEmployees).draw();
}
function loadEmployeesToInput(paramEmployees) {
  console.log(paramEmployees);
  gId = paramEmployees.id;
  $("#input-employees-name").val(paramEmployees.contactName);
  $("#input-employees-title").val(paramEmployees.contactTitle);
  $("#input-employees-address").val(paramEmployees.address);
  $("#input-mobile").val(paramEmployees.mobile);
  $("#input-email").val(paramEmployees.email);
  $("#input-note").val(paramEmployees.note);
  $("#input-createBy").val(paramEmployees.createBy);
  $("#input-updateBy").val(paramEmployees.updateBy);
  $("#input-createDate").val(changDataDate(paramEmployees.createDate));
  $("#input-updateDate").val(changDataDate(paramEmployees.updateDate));
}
function getDataInput(paramEmployees) {
  paramEmployees.contactName = $("#input-employees-name").val();
  paramEmployees.contactTitle = $("#input-employees-title").val();
  paramEmployees.address = $("#input-employees-address").val();
  paramEmployees.mobile = $("#input-mobile").val();
  paramEmployees.email = $("#input-email").val();
  paramEmployees.note = $("#input-note").val();
  paramEmployees.createBy = $("#input-createBy").val();
  paramEmployees.updateBy = $("#input-updateBy").val();
}
function validateEmployees(paramEmployees) {
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  if (paramEmployees.contactName == "" || paramEmployees.mobile == "") {
    alert("Không để trống tên khách hàng và số điện thoại trước khi save");
    return false;
  }
  else if (paramEmployees.email == null && !emailRegex.test(paramEmployees.email)) {
    alert("Email không hợp lệ");
    return false;
  }
  else {
    return true
  }
}
function onConfirmDeleteEmployeesClick() {
  if (gId != 0) {
    $.ajax({
      url: gURL + "/" + gId,
      method: "DELETE",
      success: () => {
        alert("Employees id: " + gId + " successfully delete");
        $('#modal-delete-employees').modal('hide');
        onLoadPage();
      }
    })
  }
  else {
    $.ajax({
      url: gURL,
      method: "DELETE",
      success: () => {
        alert(" All Employees successfully delete");
        $('#modal-delete-employees').modal('hide');
        onLoadPage();
      }
    })
  }
}
function changDataDate(paramDate) {
  var dateObject = new Date(paramDate);
  return dateObject.toISOString().slice(0, 16);
}