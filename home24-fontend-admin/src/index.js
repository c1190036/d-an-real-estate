'use strict';
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gURL = "http://localhost:8080/realestate";
var gRealeStateId = 0;
var gProvinceId = 0;
var gDistrictId = 0;
var gWardId = 0;
var gStreetId = 0;
var gProjectId = -1;
var gCustomerId = 0;
let realestateTable = $("#realestate-table").DataTable({
  columns: [
    { data: "id" },
    {
      data: "type",
      render: function (data, type, row) {
        switch (data) {
          case 0:
            return "Đất";
          case 1:
            return "Nhà ở";
          case 2:
            return "Căn hộ/Chung cư";
          case 3:
            return "Văn phòng, Mặt bằng";
          case 4:
            return "Kinh doanh";
          case 5:
            return "Phòng trọ";
          default:
            return "Không xác định";
        }
      }
    },
    {
      data: "request",
      render: function (data, type, row) {
        switch (data) {
          case 0:
            return "Cần bán";
          case 2:
            return "Cần mua";
          case 3:
            return "Cho thuê";
          case 4:
            return "Cần thuê";
          default:
            return "Không xác định";
        }
      }
    },
    {
      data: "project",
      render: function (data, type, row) {
        if (data && data.district && data.district.province && data.district.province.name) {
          return data.district.province.name; // Trả về tên tỉnh thành
        } else {
          return ""; // Không hiển thị gì nếu dữ liệu là null
        }
      }
    },
    {
      data: "project",
      render: function (data, type, row) {
        if (data && data.district && data.district.name) {
          return data.district.name; // Trả về tên quận huyện
        } else {
          return ""; // Không hiển thị gì nếu dữ liệu là null
        }
      }
    },
    {
      data: "project",
      render: function (data, type, row) {
        if (data && data.ward && data.ward.prefix && data.ward.name) {
          return data.ward.prefix + " " + data.ward.name; // Trả về prefix+name của ward
        } else {
          return ""; // Không hiển thị gì nếu dữ liệu là null hoặc không có ward
        }
      }
    },
    {
      data: "project",
      render: function (data, type, row) {
        if (data && data.street && data.street.prefix && data.street.name) {
          return data.street.prefix + " " + data.street.name; // Trả về prefix+name của street
        } else {
          return ""; // Không hiển thị gì nếu dữ liệu là null
        }
      }
    },
    {
      data: "project",
      render: function (data, type, row) {
        if (data && data.name) {
          return data.name; // Trả về name của project
        } else {
          return ""; // Không hiển thị gì nếu dữ liệu là null
        }
      }
    },
    { data: 'address' },
    {
      data: "customers",
      render: function (data, type, row) {
        if (data && data.contactName) {
          return data.contactName; // Trả về name của customer
        } else {
          return ""; // Không hiển thị    gì nếu dữ liệu là null
        }
      }
    },
    { data: 'price' },
    {
      data: 'dateCreate',
      render: function (data, type, row) {
        return changDataDate(data);
      }
    },
    { data: 'acreage' },
    { data: 'description' },
    { data: 'apartCode' },
    { data: 'photo' },
    { data: 'action' },
  ],
  columnDefs: [
    {
      targets: -1,
      defaultContent: `<i class="fas fa-edit text-primary"></i>
      | <i class="fas fa-trash text-danger"></i>`,
    },
  ],
})

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$("#realestate-table").on("click", ".fa-edit", onUpdateRealeStateClick);
$("#realestate-table").on("click", ".fa-trash", onDeleteRealeStateClick);
$("#create-reale-state").on("click", onPostRealeStateClick);
$("#update-reale-state").on("click", onPutRealeStateClick);
$("#delete-realestate").on("click", onConfirmDeleteRealeStateClick);
$("#delete-all-reale-state").on("click", onDeleteAllRealeStateClick);
$("#select-province").on("change", onProvinceChange);
$("#select-district").on("change", onDistrictChange);
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// hàm xử lý sự kiện tải trang
$(document).ready(function () {
  onLoadPage();
})
function onLoadPage() {
  gRealeStateId = 0;
  gProvinceId = 0;
  gDistrictId = 0;
  gWardId = 0;
  gStreetId = 0;
  gProjectId = -1;
  gCustomerId = 0;
  $(".realestate-class input").val("");
  $("#select-province").empty();
  $("#select-district").empty();
  $("#select-ward").empty();
  $("#select-street").empty();
  const optionProvince = $("<option>").val(0).text("Chọn Tỉnh(Thành Phố)").appendTo("#select-province");
  const optionDistrict = $("<option>").val(0).text("Chọn Quận(Huyện)").appendTo("#select-district");
  const optionWard = $("<option>").val(0).text("Chọn Phường").appendTo("#select-ward");
  const optionStreet = $("<option>").val(0).text("Chọn Đường").appendTo("#select-street");
  const optionProject = $("<option>").val(-1).text("Chọn Project").appendTo("#select-project");
  const optionCustomer = $("<option>").val(0).text("Chọn Khách Hàng").appendTo("#select-customer");

  $.get(gURL, loadRealeStateToTable);
  $.get("http://localhost:8080/province", loadProvinceToSelect);
  $.get("http://localhost:8080/project", loadProjectToSelect);
  $.get("http://localhost:8080/customers", loadCustomerToSelect);
}
function onUpdateRealeStateClick() {
  let vSelectRow = $(this).parents('tr');
  let vSelectedData = realestateTable.row(vSelectRow).data();
  gRealeStateId = vSelectedData.id;
  $.get(gURL + "/" + gRealeStateId, loadRealeStateToInput);
}
function onDeleteRealeStateClick() {
  let vSelectRow = $(this).parents('tr');
  let vSelectedData = realestateTable.row(vSelectRow).data();
  gRealeStateId = vSelectedData.id;
  $("#modal-delete-realestate").modal('show');
}
function onPostRealeStateClick() {
  var vRealeState = {
    type: "",
    request: "",
    address: "",
    price: "",
    acreage: "",
    description: "",
    apartCode: "",
    photo: "",
  }
  getDataInput(vRealeState);
  var vResult = validateRealeState(vRealeState);
  if (vResult) {
    $.ajax({
      url: gURL + "?customerId=" + gCustomerId + "&wardId=" + gWardId + "&streetId=" + gStreetId + "&projectId=" + gProjectId,
      method: 'POST',
      data: JSON.stringify(vRealeState),
      contentType: 'application/json',
      success: () => {
        alert('RealeState created successfully');
        onLoadPage();
      },
      error: (err) => alert(err.responseText),
    });
  }
}
function onPutRealeStateClick() {
  var vRealeState = {
    type: "",
    request: "",
    address: "",
    price: "",
    acreage: "",
    description: "",
    apartCode: "",
    //photo: "",
  }
  getDataInput(vRealeState);
  var vResult = validateRealeState(vRealeState);
  console.log(gURL+"/"+gRealeStateId + "?customerId=" + gCustomerId + "&wardId=" + gWardId + "&streetId=" + gStreetId + "&projectId=" + gProjectId);
  if (vResult) {
    $.ajax({
      url: gURL+"/"+gRealeStateId + "?customerId=" + gCustomerId + "&wardId=" + gWardId + "&streetId=" + gStreetId + "&projectId=" + gProjectId,
      method: 'PUT',
      data: JSON.stringify(vRealeState),
      contentType: 'application/json',
      success: () => {
        alert('RealeState update successfully');
        onLoadPage();
      },
      error: (err) => alert(err.responseText),
    });
  }
}
function onDeleteAllRealeStateClick() {
  gRealeStateId = 0;
  $("#modal-delete-project").modal('show');
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function loadRealeStateToTable(paramRealeState) {
  console.log(paramRealeState);
  realestateTable.clear().rows.add(paramRealeState).draw();
}

/*<Load to select> */
function loadProvinceToSelect(paramProvince) {
  $.each(paramProvince, function (index, province) {
    const option = $("<option>")
      .val(province.id)
      .text(province.name);
    $("#select-province").append(option);
  });
}
function onProvinceChange() {
  gProvinceId = $(this).val();
  $.get("http://localhost:8080/province/" + gProvinceId + "/district", loadDistrictToSelect);
}
function onDistrictChange() {
  gDistrictId = $(this).val();
  $.get("http://localhost:8080/province/district/" + gDistrictId + "/ward", loadWardToSelect);
  $.get("http://localhost:8080/province/district/" + gDistrictId + "/street", loadStreetToSelect);
}

function loadDistrictToSelect(paramDistrict) {
  $("#select-district").empty();
  $.each(paramDistrict, function (index, district) {
    const option = $("<option>")
      .val(district.id)
      .text(district.prefix + " " + district.name);
    $("#select-district").append(option);
  });
}
function loadWardToSelect(parramWard) {
  $("#select-ward").empty();
  $.each(parramWard, function (index, ward) {
    const option = $("<option>")
      .val(ward.id)
      .text(ward.prefix + " " + ward.name);
    $("#select-ward").append(option);
  });
}
function loadStreetToSelect(parramStreet) {
  $("#select-street").empty();
  $.each(parramStreet, function (index, street) {
    const option = $("<option>")
      .val(street.id)
      .text(street.prefix + " " + street.name);
    $("#select-street").append(option);
  });
}
function loadProjectToSelect(paramProject) {
  $.each(paramProject, function (index, project) {
    const option = $("<option>")
      .val(project.id)
      .text(project.name);
    $("#select-project").append(option);
  });
}
function loadCustomerToSelect(paramCustomer) {
  $.each(paramCustomer, function (index, customer) {
    const option = $("<option>")
      .val(customer.id)
      .text(customer.contactName);
    $("#select-customer").append(option);
  });
}
/*</Load to select> */
function loadRealeStateToInput(paramRealeState) {
  console.log(paramRealeState);
  gRealeStateId = paramRealeState.id;
  $("#select-type").val(paramRealeState.type);
  $("#select-request").val(paramRealeState.request);
  $("#select-province").val(paramRealeState.provinceId);
  // Load district data
  $.get("http://localhost:8080/province/" + paramRealeState.provinceId + "/district", function (districtData) {
    loadDistrictToSelect(districtData);

    if (paramRealeState.districtId) {
      $("#select-district").val(paramRealeState.districtId);
      // Load ward data
      $.get("http://localhost:8080/province/district/" + paramRealeState.districtId + "/ward", function (wardData) {
        loadWardToSelect(wardData);

        if (paramRealeState.ward) {
          $("#select-ward").val(paramRealeState.ward.id);

          // Load street data
          $.get("http://localhost:8080/province/district/" + paramRealeState.districtId + "/street", function (streetData) {
            loadStreetToSelect(streetData);

            if (paramRealeState.streetId) {
              $("#select-street").val(paramRealeState.streetId);

              // Continue with setting other values
              $("#select-project").val(paramRealeState.project.id);
              $("#input-address").val(paramRealeState.address);
              $("#select-customer").val(paramRealeState.customers.id);
              $("#input-price").val(paramRealeState.price);
              $("#input-date-create").val(changDataDate(paramRealeState.dateCreate));
              $("#input-acreage").val(paramRealeState.acreage);
              $("#input-description").val(paramRealeState.description);
              $("#input-apart-code").val(paramRealeState.apartCode);
              $("#input-wall-area").val(paramRealeState.wallArea);
              $("#input-photo").val(paramRealeState.photo);
              // ...
            }
          });
        } else {
          // Continue with setting other values
          $("#select-project").val(paramRealeState.project.id);
          $("#input-address").val(paramRealeState.address);
          $("#select-customer").val(paramRealeState.customers.id);
          $("#input-price").val(paramRealeState.price);
          $("#input-date-create").val(changDataDate(paramRealeState.dateCreate));
          $("#input-acreage").val(paramRealeState.acreage);
          $("#input-description").val(paramRealeState.description);
          $("#input-apart-code").val(paramRealeState.apartCode);
          $("#input-wall-area").val(paramRealeState.wallArea);
          $("#input-photo").val(paramRealeState.photo);
          // ...
        }
      });
    } else {
      // Continue with setting other values
      $("#select-project").val(paramRealeState.project.id);
      $("#input-address").val(paramRealeState.address);
      $("#select-customer").val(paramRealeState.customers.id);
      $("#input-price").val(paramRealeState.price);
      $("#input-date-create").val(changDataDate(paramRealeState.dateCreate));
      $("#input-acreage").val(paramRealeState.acreage);
      $("#input-description").val(paramRealeState.description);
      $("#input-apart-code").val(paramRealeState.apartCode);
      $("#input-wall-area").val(paramRealeState.wallArea);
      $("#input-photo").val(paramRealeState.photo);
      // ...
    }
  });
}
function getDataInput(paramRealeState) {
  gProvinceId = $("#select-province").val();
  gDistrictId = $("#select-district").val();
  gWardId = $("#select-ward").val();
  gStreetId = $("#select-street").val();
  gProjectId = $("#select-project").val();
  gCustomerId = $("#select-customer").val();
  paramRealeState.type = $("#select-type").val();
  paramRealeState.request = $("#select-request").val();
  paramRealeState.address = $("#input-address").val();
  paramRealeState.description = $("#input-project-description").val();
  paramRealeState.apartCode = $("#input-apart-code").val();
  paramRealeState.price = $("#input-price").val();
  paramRealeState.acreage = $("#input-acreage").val();
  //paramRealeState.photo=$("#input-photo").val();
}
function validateRealeState(paramRealeState) {
  if (gProjectId == -1 && gStreetId == 0 && gWardId == 0) {
    alert("Chọn phường và đường nếu không phải là dự án có sẵn hoặc chọn dự án có sẵn");
    return false;
  }
  if (gCustomerId == 0) {
    alert("Chọn khách hàng trước khi save");
    return false;
  }
  if (paramRealeState.type == "") {
    alert("Chọn loại bất động sản trước khi save");
    return false;
  }
  if (paramRealeState.request == "") {
    alert("Chọn loại nhu cầu trước khi save");
    return false;
  }
  else {
    return true
  }
}
function onConfirmDeleteRealeStateClick() {
  if (gRealeStateId != 0) {
    $.ajax({
      url: gURL + "/" + gRealeStateId,
      method: "DELETE",
      success: () => {
        alert("RealeState id: " + gRealeStateId + " successfully delete");
        $('#modal-delete-realestate').modal('hide');
        onLoadPage();
      }
    })
  }
  else {
    $.ajax({
      url: gURL,
      method: "DELETE",
      success: () => {
        alert(" All RealeState successfully delete");
        $('#modal-delete-realestate').modal('hide');
        onLoadPage();
      }
    })
  }
}

function changDataDate(paramDate) {
  var dateObject = new Date(paramDate);
  return dateObject.toISOString().slice(0, 16);
}


