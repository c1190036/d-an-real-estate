
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gURL = "http://localhost:8080/customers";
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$("#btn-login").on("click", onLoginClick);

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
$(document).ready(function () {
});
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function onLoginClick() {
    var vCustomer = {
        username: "",
        password: ""
    }
    getDataInput(vCustomer);
    var vResult = validateData(vCustomer);

    if (vResult) {
        console.log(gURL + "/username/" + vCustomer.username + "/password/" + vCustomer.password);
        $.get(gURL + "/username/" + vCustomer.username + "/password/" + vCustomer.password,
            function (data) {
                console.log(data);
                CheckData(data);
            })
            .fail(function (err, textStatus, errorThrown) {
                alert("Sai username hoặc mật khẩu");
            });
    }
}
function getDataInput(paramCustomer) {
    paramCustomer.username = $("#inp-username").val();
    paramCustomer.password = $("#inp-password").val();
}
function validateData(paramCustomer) {
    if (paramCustomer.username == "" || paramCustomer.password == "") {
        alert("Không để trống trước khi đăng nhập");
        return false;
    }
    else {
        return true;
    }
}
function CheckData(paramResult) {
    // Lưu thông tin đăng nhập vào LocalStorage
    localStorage.setItem('loggedIn', 'true');
    localStorage.setItem('username', paramResult.username);
    window.location.href = "index.html";
}