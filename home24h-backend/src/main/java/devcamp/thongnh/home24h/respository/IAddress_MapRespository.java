package devcamp.thongnh.home24h.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import devcamp.thongnh.home24h.model.CAddress_Map;

public interface IAddress_MapRespository extends JpaRepository<CAddress_Map, Long> {
    
}
