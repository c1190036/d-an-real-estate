package devcamp.thongnh.home24h.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import devcamp.thongnh.home24h.model.CCustomers;
import devcamp.thongnh.home24h.respository.ICustomerRespository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CustomerController {
    @Autowired
    ICustomerRespository pCustomerRespository;

    // Get All Data Customer By Table
    @GetMapping("/customers")
    public ResponseEntity<List<CCustomers>> getAllCustomer() {
        try {
            List<CCustomers> customers = pCustomerRespository.findAll();
            return new ResponseEntity<>(customers, HttpStatus.OK);

        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Data Customer By Id
    @GetMapping("/customers/{id}")
    public ResponseEntity<CCustomers> getCustomerById(@PathVariable("id") long pId) {
        try {
            Optional<CCustomers> customerId = pCustomerRespository.findById(pId);
            if (customerId.isPresent()) {
                return new ResponseEntity<>(customerId.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Check Customer By Username
    @GetMapping("/customers/username/{username}/password/{password}")
    public ResponseEntity<Object> checkCustomerByUsername(@PathVariable("username") String pUserName,@PathVariable("password") String confirmPassword) {
        try {
            Optional<CCustomers> customerUsername = pCustomerRespository.findByUsername(pUserName);
            if(customerUsername.isPresent()&&customerUsername.get().getPassword().equals(confirmPassword)){
                return new ResponseEntity<>(customerUsername.get(), HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>(false,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Post Data Customer
    @PostMapping("/customers")
    public ResponseEntity<Object> postCustomer(@RequestBody CCustomers pCustomer) {
        try {
            Optional<CCustomers> customerEmail = pCustomerRespository.findByEmail(pCustomer.getEmail());
            Optional<CCustomers> customerMobie = pCustomerRespository.findByMobile(pCustomer.getMobile());
            Optional<CCustomers> customerUsername = pCustomerRespository.findByUsername(pCustomer.getUsername());
            if (customerEmail.isPresent() || customerMobie.isPresent() || customerUsername.isPresent()) {
                return new ResponseEntity<>("Mobile number or Email or Username already exists", HttpStatus.CONFLICT);
            } else {
                pCustomer.setCreateDate(new Date());
                pCustomer.setUpdateDate(new Date());
                CCustomers customer = pCustomerRespository.save(pCustomer);
                return new ResponseEntity<Object>(customer, HttpStatus.CREATED);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Put Data Customer By Id
    @PutMapping("/customers/{id}")
    public ResponseEntity<Object> putCustomerById(@RequestBody CCustomers pCustomer, @PathVariable("id") long pId) {
        try {
            Optional<CCustomers> customerEmail = pCustomerRespository.findByEmail(pCustomer.getEmail());
            Optional<CCustomers> customerMobie = pCustomerRespository.findByMobile(pCustomer.getMobile());
            Optional<CCustomers> customerId = pCustomerRespository.findById(pId);
            if (!customerId.isPresent()) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else if (customerEmail.isPresent() && customerEmail.get().getId() != pId) {
                return new ResponseEntity<>("Email already exists", HttpStatus.CONFLICT);
            } else if (customerMobie.isPresent() && customerMobie.get().getId() != pId) {
                return new ResponseEntity<>("Mobile numberalready exists", HttpStatus.CONFLICT);
            } else {
                CCustomers customersUpdate = customerId.get();
                customersUpdate.setContactName(pCustomer.getContactName());
                customersUpdate.setContactTitle(pCustomer.getContactTitle());
                customersUpdate.setAddress(pCustomer.getAddress());
                customersUpdate.setMobile(pCustomer.getMobile());
                customersUpdate.setEmail(pCustomer.getEmail());
                customersUpdate.setNote(pCustomer.getNote());
                customersUpdate.setUpdateBy(pCustomer.getUpdateBy());
                customersUpdate.setUpdateDate(new Date());
                pCustomerRespository.save(customersUpdate);
                return new ResponseEntity<>(customersUpdate, HttpStatus.OK);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete Data Customer By Id
    @DeleteMapping("/customers/{id}")
    public ResponseEntity<Object> deleteCustomerById(@PathVariable("id") long pId) {
        try {
            Optional<CCustomers> customerId = pCustomerRespository.findById(pId);
            if (customerId.isPresent()) {
                pCustomerRespository.deleteById(pId);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete All Data Customer
    @DeleteMapping("/customers")
    public ResponseEntity<Object> deleteAllCustomer() {
        try {
            pCustomerRespository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
