package devcamp.thongnh.home24h.model;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "project")
public class CProject {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "_name", length = 200)
    private String name;

    @Column(name = "_province_id")
    private Long provinceId;

    @ManyToOne
    @JoinColumn(name = "_district_id")
    @JsonIgnoreProperties("project")
    private CDistrict district;

    @ManyToOne
    @JoinColumn(name = "_ward_id")
    @JsonIgnoreProperties("project")
    private CWard ward;

    @ManyToOne
    @JoinColumn(name = "_street_id")
    @JsonIgnoreProperties("project")
    private CStreet street;

    @Column(name = "address", length = 1000)
    private String address;

    @Column(name = "slogan", columnDefinition = "MEDIUMTEXT")
    private String slogan;

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;

    @Column(name = "acreage", precision = 20, scale = 2)
    private BigDecimal acreage;

    @Column(name = "construct_area", precision = 20, scale = 2)
    private BigDecimal constructArea;

    @Column(name = "num_block")
    private Long numBlock;

    @Column(name = "num_floors", length = 500)
    private String numFloors;

    @Column(name = "num_apartment")
    private Long numApartment;

    @Column(name = "apartment_area", length = 500)
    private String apartmentArea;

    @Column(name = "investor")
    private Long investor;

    @Column(name = "construction_contractor")
    private Long constructionContractor;

    @Column(name = "design_unit")
    private Long designUnit;

    @Column(name = "utilities", length = 1000, nullable = false)
    private String utilities;

    @Column(name = "region_link", length = 1000, nullable = false)
    private String regionLink;

    @Column(name = "photo", length = 5000)
    private String photo;

    @Column(name = "_lat", columnDefinition = "DOUBLE UNSIGNED")
    private Double lat;

    @Column(name = "_lng", columnDefinition = "DOUBLE UNSIGNED")
    private Double lng;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "project")
    @JsonBackReference
    private List<CRealEstate> realEstate;

    // Constructors, getters, and setters
    public CProject() {
    }

    public CProject(Long id, String name, Long provinceId, CDistrict district, CWard ward, CStreet street,
            String address, String slogan, String description, BigDecimal acreage, BigDecimal constructArea,
            Long numBlock, String numFloors, Long numApartment, String apartmentArea, Long investor,
            Long constructionContractor, Long designUnit, String utilities, String regionLink, String photo, Double lat,
            Double lng, List<CRealEstate> realEstate) {
        this.id = id;
        this.name = name;
        this.provinceId = provinceId;
        this.district = district;
        this.ward = ward;
        this.street = street;
        this.address = address;
        this.slogan = slogan;
        this.description = description;
        this.acreage = acreage;
        this.constructArea = constructArea;
        this.numBlock = numBlock;
        this.numFloors = numFloors;
        this.numApartment = numApartment;
        this.apartmentArea = apartmentArea;
        this.investor = investor;
        this.constructionContractor = constructionContractor;
        this.designUnit = designUnit;
        this.utilities = utilities;
        this.regionLink = regionLink;
        this.photo = photo;
        this.lat = lat;
        this.lng = lng;
        this.realEstate = realEstate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }

    public CDistrict getDistrict() {
        return district;
    }

    public void setDistrict(CDistrict district) {
        this.district = district;
    }

    public CWard getWard() {
        return ward;
    }

    public void setWard(CWard ward) {
        this.ward = ward;
    }

    public CStreet getStreet() {
        return street;
    }

    public void setStreet(CStreet street) {
        this.street = street;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getAcreage() {
        return acreage;
    }

    public void setAcreage(BigDecimal acreage) {
        this.acreage = acreage;
    }

    public BigDecimal getConstructArea() {
        return constructArea;
    }

    public void setConstructArea(BigDecimal constructArea) {
        this.constructArea = constructArea;
    }

    public Long getNumBlock() {
        return numBlock;
    }

    public void setNumBlock(Long numBlock) {
        this.numBlock = numBlock;
    }

    public String getNumFloors() {
        return numFloors;
    }

    public void setNumFloors(String numFloors) {
        this.numFloors = numFloors;
    }

    public Long getNumApartment() {
        return numApartment;
    }

    public void setNumApartment(Long numApartment) {
        this.numApartment = numApartment;
    }

    public String getApartmentArea() {
        return apartmentArea;
    }

    public void setApartmentArea(String apartmentArea) {
        this.apartmentArea = apartmentArea;
    }

    public Long getInvestor() {
        return investor;
    }

    public void setInvestor(Long investor) {
        this.investor = investor;
    }

    public Long getConstructionContractor() {
        return constructionContractor;
    }

    public void setConstructionContractor(Long constructionContractor) {
        this.constructionContractor = constructionContractor;
    }

    public Long getDesignUnit() {
        return designUnit;
    }

    public void setDesignUnit(Long designUnit) {
        this.designUnit = designUnit;
    }

    public String getUtilities() {
        return utilities;
    }

    public void setUtilities(String utilities) {
        this.utilities = utilities;
    }

    public String getRegionLink() {
        return regionLink;
    }

    public void setRegionLink(String regionLink) {
        this.regionLink = regionLink;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public List<CRealEstate> getRealEstate() {
        return realEstate;
    }

    public void setRealEstate(List<CRealEstate> realEstate) {
        this.realEstate = realEstate;
    }

}
