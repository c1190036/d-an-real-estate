package devcamp.thongnh.home24h.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import devcamp.thongnh.home24h.model.CRegion_Link;
import devcamp.thongnh.home24h.respository.IRegion_LinkResponsitory;


@RequestMapping("/")
@RestController
@CrossOrigin
public class Region_LinkController {
    @Autowired
    IRegion_LinkResponsitory pIRegion_LinkResponsitory;

    // Get All Region_Link
    @GetMapping("/region-link")
    public ResponseEntity<List<CRegion_Link>> getAllRegion_Link() {
        try {
            List<CRegion_Link> regionLinkList = pIRegion_LinkResponsitory.findAll();
            return new ResponseEntity<>(regionLinkList, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Region_Link By Id
    @GetMapping("/egion-link/{id}")
    public ResponseEntity<CRegion_Link> getRegion_LinkById(@PathVariable("id") long pId) {
        try {
            Optional<CRegion_Link> regionLinkId = pIRegion_LinkResponsitory.findById(pId);
            if (regionLinkId.isPresent()) {
                return new ResponseEntity<>(regionLinkId.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Post Region_Link
    @PostMapping("/region-link")
    public ResponseEntity<Object> postRegion_Link(@RequestBody CRegion_Link pRegion_Link) {
        try {
            pIRegion_LinkResponsitory.save(pRegion_Link);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Put Region_Link By Id
    @PutMapping("/region-link/{id}")
    public ResponseEntity<Object> putRegion_LinkById(@PathVariable("id") long pId, @RequestBody CRegion_Link pRegion_Link) {
        try {
            Optional<CRegion_Link> regionLinkId = pIRegion_LinkResponsitory.findById(pId);
            if (regionLinkId.isPresent()) {
                CRegion_Link regionLinkUpdate = regionLinkId.get();
                pIRegion_LinkResponsitory.save(regionLinkUpdate);
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete Region_Link By Id
    @DeleteMapping("/region-link/{id}")
    public ResponseEntity<Object> deleteRegion_LinkById(@PathVariable("id") long pId) {
        try {
            Optional<CRegion_Link> regionLinkId = pIRegion_LinkResponsitory.findById(pId);
            if (regionLinkId.isPresent()) {
                pIRegion_LinkResponsitory.deleteById(pId);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete All Region_Link
    @DeleteMapping("/region-link")
    public ResponseEntity<Object> deleteAllRegion_Link() {
        try {
            pIRegion_LinkResponsitory.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
