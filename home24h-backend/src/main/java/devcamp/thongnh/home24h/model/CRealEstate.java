package devcamp.thongnh.home24h.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "realestate")
public class CRealEstate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "title", length = 2000)
    private String title;

    @Column(name = "type")
    private Long type;

    @Column(name = "request")
    private Long request;

    @Column(name = "province_id")
    private Long provinceId;

    @Column(name = "district_id")
    private Long districtId;

    @ManyToOne(optional = true)
    @JoinColumn(name = "wards_id")
    @JsonIgnoreProperties("realEstate")
    private CWard ward;

    @Column(name = "street_id")
    private Long streetId;

    @ManyToOne
    @JoinColumn(name = "project_id")
    @JsonIgnoreProperties("realEstate")
    private CProject project;

    @Column(name = "address", nullable = false, length = 2000)
    private String address;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    @JsonIgnoreProperties("realEstate")
    private CCustomers customers;

    @Column(name = "price")
    private Long price;

    @Column(name = "price_min")
    private Long priceMin;

    @Column(name = "price_time")
    private Long priceTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_create")
    private Date dateCreate;

    @Column(name = "acreage")
    private BigDecimal acreage;

    @Column(name = "direction")
    private Long direction;

    @Column(name = "total_floors")
    private Long totalFloors;

    @Column(name = "number_floors")
    private Long numberFloors;

    @Column(name = "bath")
    private Long bath;

    @Column(name = "apart_code", length = 10)
    private String apartCode;

    @Column(name = "wall_area")
    private BigDecimal wallArea;

    @Column(name = "bedroom")
    private Long bedroom;

    @Column(name = "balcony")
    private Long balcony;

    @Column(name = "landscape_view", length = 255)
    private String landscapeView;

    @Column(name = "apart_loca")
    private Long apartLoca;

    @Column(name = "apart_type")
    private Long apartType;

    @Column(name = "furniture_type")
    private Long furnitureType;

    @Column(name = "price_rent")
    private Long priceRent;

    @Column(name = "return_rate")
    private Double returnRate;

    @Column(name = "legal_doc")
    private Long legalDoc;

    @Column(name = "description", length = 2000)
    private String description;

    @Column(name = "width_y")
    private Long widthY;

    @Column(name = "long_x")
    private Long longX;

    @Column(name = "street_house")
    private Long streetHouse;

    @Column(name = "FSBO")
    private Long fsbo;

    @Column(name = "view_num")
    private Long viewNum;

    @Column(name = "create_by")
    private Long createBy;

    @Column(name = "update_by")
    private Long updateBy;

    @Column(name = "shape", length = 200)
    private String shape;

    @Column(name = "distance2facade")
    private Long distance2facade;

    @Column(name = "adjacent_facade_num")
    private Long adjacentFacadeNum;

    @Column(name = "adjacent_road", length = 200)
    private String adjacentRoad;

    @Column(name = "alley_min_width")
    private Long alleyMinWidth;

    @Column(name = "adjacent_alley_min_width")
    private Long adjacentAlleyMinWidth;

    @Column(name = "factor")
    private Long factor;

    @Column(name = "structure", length = 2000)
    private String structure;

    @Column(name = "DTSXD")
    private Long dtsxd;

    @Column(name = "CLCL")
    private Long clcl;

    @Column(name = "CTXD_price")
    private Long ctxdPrice;

    @Column(name = "CTXD_value")
    private Long ctxdValue;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "realestate")
    private List<CPhoto> photo;

    @Column(name = "_lat")
    private Double lat;

    @Column(name = "_lng")
    private Double lng;
    // Constructors, getters, setters, etc.

    public CRealEstate() {
    }

    public CRealEstate(Long id, String title, Long type, Long request, Long provinceId, Long districtId, CWard ward,
            Long streetId, CProject project, String address, CCustomers customers, Long price, Long priceMin,
            Long priceTime, Date dateCreate, BigDecimal acreage, Long direction, Long totalFloors, Long numberFloors,
            Long bath, String apartCode, BigDecimal wallArea, Long bedroom, Long balcony, String landscapeView,
            Long apartLoca, Long apartType, Long furnitureType, Long priceRent, Double returnRate, Long legalDoc,
            String description, Long widthY, Long longX, Long streetHouse, Long fsbo, Long viewNum, Long createBy,
            Long updateBy, String shape, Long distance2facade, Long adjacentFacadeNum, String adjacentRoad,
            Long alleyMinWidth, Long adjacentAlleyMinWidth, Long factor, String structure, Long dtsxd, Long clcl,
            Long ctxdPrice, Long ctxdValue, List<CPhoto> photo, Double lat, Double lng) {
        this.id = id;
        this.title = title;
        this.type = type;
        this.request = request;
        this.provinceId = provinceId;
        this.districtId = districtId;
        this.ward = ward;
        this.streetId = streetId;
        this.project = project;
        this.address = address;
        this.customers = customers;
        this.price = price;
        this.priceMin = priceMin;
        this.priceTime = priceTime;
        this.dateCreate = dateCreate;
        this.acreage = acreage;
        this.direction = direction;
        this.totalFloors = totalFloors;
        this.numberFloors = numberFloors;
        this.bath = bath;
        this.apartCode = apartCode;
        this.wallArea = wallArea;
        this.bedroom = bedroom;
        this.balcony = balcony;
        this.landscapeView = landscapeView;
        this.apartLoca = apartLoca;
        this.apartType = apartType;
        this.furnitureType = furnitureType;
        this.priceRent = priceRent;
        this.returnRate = returnRate;
        this.legalDoc = legalDoc;
        this.description = description;
        this.widthY = widthY;
        this.longX = longX;
        this.streetHouse = streetHouse;
        this.fsbo = fsbo;
        this.viewNum = viewNum;
        this.createBy = createBy;
        this.updateBy = updateBy;
        this.shape = shape;
        this.distance2facade = distance2facade;
        this.adjacentFacadeNum = adjacentFacadeNum;
        this.adjacentRoad = adjacentRoad;
        this.alleyMinWidth = alleyMinWidth;
        this.adjacentAlleyMinWidth = adjacentAlleyMinWidth;
        this.factor = factor;
        this.structure = structure;
        this.dtsxd = dtsxd;
        this.clcl = clcl;
        this.ctxdPrice = ctxdPrice;
        this.ctxdValue = ctxdValue;
        this.photo = photo;
        this.lat = lat;
        this.lng = lng;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Long getRequest() {
        return request;
    }

    public void setRequest(Long request) {
        this.request = request;
    }

    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }

    public Long getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Long districtId) {
        this.districtId = districtId;
    }

    public CWard getWard() {
        return ward;
    }

    public void setWard(CWard ward) {
        this.ward = ward;
    }

    public Long getStreetId() {
        return streetId;
    }

    public void setStreetId(Long streetId) {
        this.streetId = streetId;
    }

    public CProject getProject() {
        return project;
    }

    public void setProject(CProject project) {
        this.project = project;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public CCustomers getCustomers() {
        return customers;
    }

    public void setCustomers(CCustomers customers) {
        this.customers = customers;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getPriceMin() {
        return priceMin;
    }

    public void setPriceMin(Long priceMin) {
        this.priceMin = priceMin;
    }

    public Long getPriceTime() {
        return priceTime;
    }

    public void setPriceTime(Long priceTime) {
        this.priceTime = priceTime;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public BigDecimal getAcreage() {
        return acreage;
    }

    public void setAcreage(BigDecimal acreage) {
        this.acreage = acreage;
    }

    public Long getDirection() {
        return direction;
    }

    public void setDirection(Long direction) {
        this.direction = direction;
    }

    public Long getTotalFloors() {
        return totalFloors;
    }

    public void setTotalFloors(Long totalFloors) {
        this.totalFloors = totalFloors;
    }

    public Long getNumberFloors() {
        return numberFloors;
    }

    public void setNumberFloors(Long numberFloors) {
        this.numberFloors = numberFloors;
    }

    public Long getBath() {
        return bath;
    }

    public void setBath(Long bath) {
        this.bath = bath;
    }

    public String getApartCode() {
        return apartCode;
    }

    public void setApartCode(String apartCode) {
        this.apartCode = apartCode;
    }

    public BigDecimal getWallArea() {
        return wallArea;
    }

    public void setWallArea(BigDecimal wallArea) {
        this.wallArea = wallArea;
    }

    public Long getBedroom() {
        return bedroom;
    }

    public void setBedroom(Long bedroom) {
        this.bedroom = bedroom;
    }

    public Long getBalcony() {
        return balcony;
    }

    public void setBalcony(Long balcony) {
        this.balcony = balcony;
    }

    public String getLandscapeView() {
        return landscapeView;
    }

    public void setLandscapeView(String landscapeView) {
        this.landscapeView = landscapeView;
    }

    public Long getApartLoca() {
        return apartLoca;
    }

    public void setApartLoca(Long apartLoca) {
        this.apartLoca = apartLoca;
    }

    public Long getApartType() {
        return apartType;
    }

    public void setApartType(Long apartType) {
        this.apartType = apartType;
    }

    public Long getFurnitureType() {
        return furnitureType;
    }

    public void setFurnitureType(Long furnitureType) {
        this.furnitureType = furnitureType;
    }

    public Long getPriceRent() {
        return priceRent;
    }

    public void setPriceRent(Long priceRent) {
        this.priceRent = priceRent;
    }

    public Double getReturnRate() {
        return returnRate;
    }

    public void setReturnRate(Double returnRate) {
        this.returnRate = returnRate;
    }

    public Long getLegalDoc() {
        return legalDoc;
    }

    public void setLegalDoc(Long legalDoc) {
        this.legalDoc = legalDoc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getWidthY() {
        return widthY;
    }

    public void setWidthY(Long widthY) {
        this.widthY = widthY;
    }

    public Long getLongX() {
        return longX;
    }

    public void setLongX(Long longX) {
        this.longX = longX;
    }

    public Long getStreetHouse() {
        return streetHouse;
    }

    public void setStreetHouse(Long streetHouse) {
        this.streetHouse = streetHouse;
    }

    public Long getFsbo() {
        return fsbo;
    }

    public void setFsbo(Long fsbo) {
        this.fsbo = fsbo;
    }

    public Long getViewNum() {
        return viewNum;
    }

    public void setViewNum(Long viewNum) {
        this.viewNum = viewNum;
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public Long getDistance2facade() {
        return distance2facade;
    }

    public void setDistance2facade(Long distance2facade) {
        this.distance2facade = distance2facade;
    }

    public Long getAdjacentFacadeNum() {
        return adjacentFacadeNum;
    }

    public void setAdjacentFacadeNum(Long adjacentFacadeNum) {
        this.adjacentFacadeNum = adjacentFacadeNum;
    }

    public String getAdjacentRoad() {
        return adjacentRoad;
    }

    public void setAdjacentRoad(String adjacentRoad) {
        this.adjacentRoad = adjacentRoad;
    }

    public Long getAlleyMinWidth() {
        return alleyMinWidth;
    }

    public void setAlleyMinWidth(Long alleyMinWidth) {
        this.alleyMinWidth = alleyMinWidth;
    }

    public Long getAdjacentAlleyMinWidth() {
        return adjacentAlleyMinWidth;
    }

    public void setAdjacentAlleyMinWidth(Long adjacentAlleyMinWidth) {
        this.adjacentAlleyMinWidth = adjacentAlleyMinWidth;
    }

    public Long getFactor() {
        return factor;
    }

    public void setFactor(Long factor) {
        this.factor = factor;
    }

    public String getStructure() {
        return structure;
    }

    public void setStructure(String structure) {
        this.structure = structure;
    }

    public Long getDtsxd() {
        return dtsxd;
    }

    public void setDtsxd(Long dtsxd) {
        this.dtsxd = dtsxd;
    }

    public Long getClcl() {
        return clcl;
    }

    public void setClcl(Long clcl) {
        this.clcl = clcl;
    }

    public Long getCtxdPrice() {
        return ctxdPrice;
    }

    public void setCtxdPrice(Long ctxdPrice) {
        this.ctxdPrice = ctxdPrice;
    }

    public Long getCtxdValue() {
        return ctxdValue;
    }

    public void setCtxdValue(Long ctxdValue) {
        this.ctxdValue = ctxdValue;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public List<CPhoto> getPhoto() {
        return photo;
    }

    public void setPhoto(List<CPhoto> photo) {
        this.photo = photo;
    }

}
