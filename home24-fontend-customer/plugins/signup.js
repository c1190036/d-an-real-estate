
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gURL = "http://localhost:8080/customers";
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$("#btn-signup").on("click", onSignUpClick);
$("#inp-mobie").on("change", function () {
    $("#inp-email").attr("placeholder", "Nếu bạn không nhập email thì sẽ không thể lấy mật khẩu nếu quên.");
})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
$(document).ready(function () {

});
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function onSignUpClick() {
    var vCustomer = {
        contactName: "",
        username: "",
        password: "",
        mobile: "",
        email: ""
    };
    getDataInput(vCustomer);
    var vResult = validateData(vCustomer);
    if (vResult) {
        $.ajax({
            url: gURL,
            method: 'POST',
            data: JSON.stringify(vCustomer),
            contentType: 'application/json',
            success: () => {
                alert('Customer created successfully');
                window.location.href = "login.html";
            },
            error: (err) => alert(err.responseText),
        });
    }

}
function getDataInput(paramCustomer) {
    paramCustomer.contactName = $("#inp-contact-name").val();
    paramCustomer.username = $("#inp-user-name").val();
    paramCustomer.password = $("#inp-password").val();
    paramCustomer.email = $("#inp-email").val();
    paramCustomer.mobile = $("#inp-mobie").val();
}
function validateData(paramCustomer) {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    console.log(paramCustomer);
    if (paramCustomer.contactName == "" || paramCustomer.username == "" || paramCustomer.password == "") {
        alert("Không để trống dữ liệu trước khi ấn");
    }
    if (paramCustomer.email != "" && !emailRegex.test(paramCustomer.email)) {
        alert("Email không đúng kiểu dữ liệu");
        return false;
    }
    if (paramCustomer.password != $("#inp-confirm-password").val()) {
        alert("Password and Confirm password không giống nhau");
        return false;
    }
    else {
        return true;
    }
}
