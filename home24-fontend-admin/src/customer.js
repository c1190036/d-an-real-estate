'use strict';
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gURL = "http://localhost:8080/customers";
var gId = 0;
let customerTable = $("#customer-table").DataTable({
  columns: [
    { data: "id" },
    { data: "contactName" },
    { data: "contactTitle" },
    { data: "address" },
    { data: "mobile" },
    { data: 'email' },
    { data: 'note' },
    { data: 'createBy' },
    { data: 'updateBy' },
    { data: 'createDate' },
    { data: 'updateDate' },
    { data: 'action' },
  ],
  columnDefs: [
    {
      targets: -1,
      defaultContent: `<i class="fas fa-edit text-primary"></i>
      | <i class="fas fa-trash text-danger"></i>`,
    },
  ],
})

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$("#customer-table").on("click", ".fa-edit", onUpdateCustomerClick);
$("#customer-table").on("click", ".fa-trash", onDeleteCustomerClick);
$("#create-customer").on("click", onPostCustomerClick);
$("#update-customer").on("click", onPutCustomerClick);
$("#delete-customer").on("click", onConfirmDeleteCustomerClick);
$("#delete-all-customer").on("click", onDeleteAllCustomerClick);
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// hàm xử lý sự kiện tải trang
$(document).ready(function () {
  onLoadPage();
})
function onLoadPage() {
  gId = 0;

  
  $("#input-customer").val("");
  $("#input-lat").val("");
  $("#input-lng").val("");
  $.get(gURL, loadCustomerToTable);
}
function onUpdateCustomerClick() {
  let vSelectRow = $(this).parents('tr');
  let vSelectedData = customerTable.row(vSelectRow).data();
  gId = vSelectedData.id;
  $.get(gURL + "/" + gId, loadCustomerToInput);
}
function onDeleteCustomerClick() {
  let vSelectRow = $(this).parents('tr');
  let vSelectedData = customerTable.row(vSelectRow).data();
  gId = vSelectedData.id;
  $("#modal-delete-customer").modal('show');
}
function onPostCustomerClick() {
  var vCustomer = {
    contactName: "",
    contactTitle: "",
    address: "",
    mobile: "",
    email: "",
    note: "",
    createBy: "",
    updateBy: "",
  }
  getDataInput(vCustomer);
  console.log(vCustomer);
  var vResult = validateCustomer(vCustomer);
  console.log(JSON.stringify(vCustomer));
  if (vResult) {
    $.ajax({
      url: gURL,
      method: 'POST',
      data: JSON.stringify(vCustomer),
      contentType: 'application/json',
      success: () => {
        alert('Customer created successfully');
        onLoadPage();
      },
      error: (err) => alert(err.responseText),
    });
  }
}
function onPutCustomerClick() {
  var vCustomer = {
    contactName: "",
    contactTitle: "",
    address: "",
    mobile: "",
    email: "",
    note: "",
    createBy: "",
    updateBy: "",
  }
  getDataInput(vCustomer);
  var vResult = validateCustomer(vCustomer);
  if (vResult) {
    $.ajax({
      url: gURL + "/" + gId,
      method: 'PUT',
      data: JSON.stringify(vCustomer),
      contentType: 'application/json',
      success: () => {
        alert('Customer update successfully');
        onLoadPage();
      },
      error: (err) => alert(err.responseText),
    });
  }
}
function onDeleteAllCustomerClick() {
  gId = 0;
  $("#modal-delete-customer").modal('show');
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/0
function loadCustomerToTable(paramCustomer) {
  customerTable.clear().rows.add(paramCustomer).draw();
}
function loadCustomerToInput(paramCustomer) {
  console.log(paramCustomer);
  gId = paramCustomer.id;
  $("#input-customer-name").val(paramCustomer.contactName);
  $("#input-customer-title").val(paramCustomer.contactTitle);
  $("#input-customer-address").val(paramCustomer.address);
  $("#input-mobile").val(paramCustomer.mobile);
  $("#input-email").val(paramCustomer.email);
  $("#input-note").val(paramCustomer.note);
  $("#input-createBy").val(paramCustomer.createBy);
  $("#input-updateBy").val(paramCustomer.updateBy);
  $("#input-createDate").val(changDataDate(paramCustomer.createDate));
  $("#input-updateDate").val(changDataDate(paramCustomer.updateDate));
}
function getDataInput(paramCustomer) {
  paramCustomer.contactName = $("#input-customer-name").val();
  paramCustomer.contactTitle = $("#input-customer-title").val();
  paramCustomer.address = $("#input-customer-address").val();
  paramCustomer.mobile = $("#input-mobile").val();
  paramCustomer.email = $("#input-email").val();
  paramCustomer.note = $("#input-note").val();
  paramCustomer.createBy = $("#input-createBy").val();
  paramCustomer.updateBy = $("#input-updateBy").val();
}
function validateCustomer(paramCustomer) {
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  if (paramCustomer.contactName == "" || paramCustomer.mobile == "") {
    alert("Không để trống tên khách hàng và số điện thoại trước khi save");
    return false;
  }
  else if (paramCustomer.email == null && !emailRegex.test(paramCustomer.email)) {
    alert("Email không hợp lệ");
    return false;
  }
  else {
    return true
  }
}
function onConfirmDeleteCustomerClick() {
  if (gId != 0) {
    $.ajax({
      url: gURL + "/" + gId,
      method: "DELETE",
      success: () => {
        alert("Customer id: " + gId + " successfully delete");
        $('#modal-delete-customer').modal('hide');
        onLoadPage();
      }
    })
  }
  else {
    $.ajax({
      url: gURL,
      method: "DELETE",
      success: () => {
        alert(" All Customer successfully delete");
        $('#modal-delete-customer').modal('hide');
        onLoadPage();
      }
    })
  }
}
function changDataDate(paramDate) {
  var dateObject = new Date(paramDate);
  return dateObject.toISOString().slice(0, 16);
}