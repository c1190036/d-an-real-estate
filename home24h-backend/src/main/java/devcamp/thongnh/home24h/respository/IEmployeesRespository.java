package devcamp.thongnh.home24h.respository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import devcamp.thongnh.home24h.model.CEmployees;

public interface IEmployeesRespository extends JpaRepository<CEmployees,Long> {
    // Kiểm tra xem username đã tồn tại trong cơ sở dữ liệu hay chưa
    Optional<CEmployees> findByUsername(String username);

    // Kiểm tra xem email đã tồn tại trong cơ sở dữ liệu hay chưa
    Optional<CEmployees> findByEmail(String email);
}
