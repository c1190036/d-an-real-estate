'use strict';
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gURL = "http://localhost:8080";
var gDistrictId = 0;
var gProvinceId = 0;
let districtTable = $("#district-table").DataTable({
  columns: [
    { data: "id" },
    { data: "name" },
    { data: "prefix" },
    {
      data: "province",
      render: function (data, type, row) {
        return data.name; // Trả về tên tỉnh thành
      }
    },
    { data: 'action' },
  ],
  columnDefs: [
    {
      targets: -1,
      defaultContent: `<i class="fas fa-edit text-primary"></i>
      | <i class="fas fa-trash text-danger"></i>`,
    },
  ],
})

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$("#district-table").on("click", ".fa-edit", onUpdateDistrictClick);
$("#district-table").on("click", ".fa-trash", onDeleteDistrictClick);
$("#create-district").on("click", onPostDistrictClick);
$("#update-district").on("click", onPutDistrictClick);
$("#delete-district").on("click", onConfirmDeleteDistrictClick);
$("#delete-all-district").on("click", onDeleteAllDistrictClick);
$("#select-province").on("change", onChangeProvinceClick);
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// hàm xử lý sự kiện tải trang
$(document).ready(function () {
  onLoadPage();
})
function onLoadPage() {
  gDistrictId = 0;
  gProvinceId = 0;
  $("#input-district-name").val("");
  $("#input-district-prefix").val("");
  $("#select-province").val(0);
  $.get(gURL + "/province/district", loadDistrictToTable);
  $.get(gURL + "/province", loadProvinceToSelect);
}
function onUpdateDistrictClick() {
  let vSelectRow = $(this).parents('tr');
  let vSelectedData = districtTable.row(vSelectRow).data();
  gDistrictId = vSelectedData.id;
  $.get(gURL + "/province/district/" + gDistrictId, loadDistrictToInput);
}
function onDeleteDistrictClick() {
  let vSelectRow = $(this).parents('tr');
  let vSelectedData = districtTable.row(vSelectRow).data();
  gDistrictId = vSelectedData.id;
  $("#modal-delete-district").modal('show');
}
function onPostDistrictClick() {
  var vDistrict = {
    name: "",
    prefix: "",
  }
  getDataInput(vDistrict);
  console.log(vDistrict);
  var vResult = validateDistrict(vDistrict);
  if (vResult) {
    $.ajax({
      url: gURL + "/province/" + gProvinceId + "/district",
      method: 'POST',
      data: JSON.stringify(vDistrict),
      contentType: 'application/json',
      success: () => {
        alert('District created successfully');
        onLoadPage();
      },
      error: (err) => alert(err.responseText),
    });
  }
}
function onPutDistrictClick() {
  var vDistrict = {
    name: "",
    prefix: "",
  }
  getDataInput(vDistrict);
  var vResult = validateDistrict(vDistrict);
  console.log(gURL + "/province/" + gProvinceId + "/district/"+gDistrictId);
  if (vResult) {
    $.ajax({  
      url: gURL + "/province/" + gProvinceId + "/district/"+gDistrictId,
      method: 'PUT',
      data: JSON.stringify(vDistrict),
      contentType: 'application/json',
      success: () => {
        alert('District update successfully');
        onLoadPage();
      },
      error: (err) => alert(err.responseText),
    });
  }
}
function onDeleteAllDistrictClick() {
  gDistrictId = 0;
  $("#modal-delete-district").modal('show');
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/0
function loadDistrictToTable(paramDistrict) {
  districtTable.clear().rows.add(paramDistrict).draw();
}
function loadProvinceToSelect(paramProvince) {
  $.each(paramProvince, function (index, province) {
    const option = $("<option>")
      .val(province.id)
      .text(province.name);

    $("#select-province").append(option);
  });
}
function loadDistrictToInput(paramDistrict) {
  console.log(paramDistrict);
  gDistrictId = paramDistrict.id;
  gProvinceId = paramDistrict.province.id;
  $("#input-district-name").val(paramDistrict.name);
  $("#input-district-prefix").val(paramDistrict.prefix);
  $("#select-province").val(paramDistrict.province.id);
}
function getDataInput(paramDistrict) {
  paramDistrict.name = $("#input-district-name").val();
  paramDistrict.prefix = $("#input-district-prefix").val();
}
function validateDistrict(paramDistrict) {
  if (paramDistrict.name == "" || paramDistrict.prefix == "") {
    alert("Không để trống dữ liệu trước kh save");
    return false;
  }
  else if (gProvinceId == 0) {
    alert("Chọn Tỉnh(Thành Phố) trước khi save");
    return false;
  }
  else {
    return true
  }
}
function onConfirmDeleteDistrictClick() {
  if (gDistrictId != 0) {
    $.ajax({
      url: gURL + "/province/district/" + gDistrictId,
      method: "DELETE",
      success: () => {
        alert("District id: " + gDistrictId + " successfully delete");
        $('#modal-delete-district').modal('hide');
        onLoadPage();
      }
    })
  }
  else {
    $.ajax({
      url: gURL + "/province/district",
      method: "DELETE",
      success: () => {
        alert(" All District successfully delete");
        $('#modal-delete-district').modal('hide');
        onLoadPage();
      }
    })
  }
}
function onChangeProvinceClick() {
  gProvinceId = $(this).val();
  console.log(gProvinceId);
}