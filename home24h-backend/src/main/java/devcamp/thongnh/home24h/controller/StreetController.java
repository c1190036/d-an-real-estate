package devcamp.thongnh.home24h.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import devcamp.thongnh.home24h.model.CDistrict;
import devcamp.thongnh.home24h.model.CProvince;
import devcamp.thongnh.home24h.model.CStreet;
import devcamp.thongnh.home24h.respository.IDistrictRespository;
import devcamp.thongnh.home24h.respository.IProvinceRespository;
import devcamp.thongnh.home24h.respository.IStreetRespository;

@RequestMapping("/")
@RestController
@CrossOrigin
public class StreetController {
    @Autowired
    IProvinceRespository pIProvinceRespository;

    @Autowired
    IDistrictRespository pDistrictRespository;

    @Autowired
    IStreetRespository pIStreetRespository;

    // Get All Street
    @GetMapping("/province/district/street")
    public ResponseEntity<List<CStreet>> getAllStreet() {
        try {
            List<CStreet> streetList = pIStreetRespository.findAll();
            return new ResponseEntity<>(streetList, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Street By Id
    @GetMapping("/province/district/street/{id}")
    public ResponseEntity<CStreet> getStreetById(@PathVariable("id") long pId) {
        try {
            Optional<CStreet> streetById = pIStreetRespository.findById(pId);
            if (streetById.isPresent()) {
                return new ResponseEntity<>(streetById.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get All Street By Id Of District
    @GetMapping("/province/district/{id}/street")
    public ResponseEntity<List<CStreet>> getAllStreetByIdOfDistrict(@PathVariable("id") long pId) {
        try {
            Optional<CDistrict> districtId = pDistrictRespository.findById(pId);
            if (districtId.isPresent()) {
                return new ResponseEntity<>(districtId.get().getStreet(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get All Street By Id Of Province
    @GetMapping("/province/{id}/district/street")
    public ResponseEntity<List<CStreet>> getAllStreetByIdOfProvince(@PathVariable("id") long pId) {
        try {
            Optional<CProvince> provinceId = pIProvinceRespository.findById(pId);
            if (provinceId.isPresent()) {
                List<CDistrict> districtList = provinceId.get().getDistrict();
                List<CStreet> streetList = new ArrayList<>();
                districtList.forEach(district -> streetList.addAll(district.getStreet()));
                return new ResponseEntity<>(streetList, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Post Street
    @PostMapping("/province/district/{id}/street")
    public ResponseEntity<Object> postStreet(@PathVariable("id") long pId, @RequestBody CStreet pStreet) {
        try {
            Optional<CDistrict> districtId = pDistrictRespository.findById(pId);
            if (districtId.isPresent()) {
                Long provinceId = districtId.get().getProvince().getId();
                pStreet.setProvince(provinceId);
                pStreet.setDistrict(districtId.get());
                pIStreetRespository.save(pStreet);
                return new ResponseEntity<>(HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Put Street
    @PutMapping("/province/district/{districtId}/street/{streetId}")
    public ResponseEntity<Object> putStreet(@PathVariable("districtId") long pDistrictId,
            @PathVariable("streetId") long pStreetId, @RequestBody CStreet pStreet) {
        try {
            Optional<CDistrict> districtId = pDistrictRespository.findById(pDistrictId);
            Optional<CStreet> streetId = pIStreetRespository.findById(pStreetId);
            if (districtId.isPresent() && streetId.isPresent()) {
                Long provinceId = districtId.get().getProvince().getId();
                pStreet.setDistrict(districtId.get());
                pStreet.setProvince(provinceId);
                pIStreetRespository.save(pStreet);
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete Street By Id
    @DeleteMapping("/province/district/street/{id}")
    public ResponseEntity<Object> deleteStreetById(@PathVariable("id") long pId) {
        try {
            Optional<CStreet> streetId = pIStreetRespository.findById(pId);
            if (streetId.isPresent()) {
                pIStreetRespository.deleteById(pId);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //Delete All Street
    @DeleteMapping("/province/district/street")
    public ResponseEntity<Object> deleteAllStreet(){
        try {
            pIStreetRespository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // TODO: handle exception
             return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
