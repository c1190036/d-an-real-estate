package devcamp.thongnh.home24h.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import devcamp.thongnh.home24h.Service.RealEstateService;
import devcamp.thongnh.home24h.model.CPhoto;
import devcamp.thongnh.home24h.model.CRealEstate;
import devcamp.thongnh.home24h.respository.IPhotoResponsitory;
import devcamp.thongnh.home24h.respository.IRealEstateResponsitory;

@RestController
@CrossOrigin
@RequestMapping
public class PhotoController {
    @Autowired
    IPhotoResponsitory pPhotoResponsitory;
    @Autowired
    IRealEstateResponsitory pRealEstateResponsitory;
    @Autowired
    RealEstateService pRealEstateService;

    // Get All Photo
    @GetMapping("/photo/all")
    public ResponseEntity<List<CPhoto>> getAllPhoto() {
        try {
            List<CPhoto> photoList = pPhotoResponsitory.findAll();
            return new ResponseEntity<List<CPhoto>>(photoList, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Photo By Id
    @GetMapping("/photo/{id}")
    public ResponseEntity<byte[]> getPhotoById(@PathVariable("id") long pId) {
        try {
            Optional<CPhoto> photo = pPhotoResponsitory.findById(pId);
            if (photo.isPresent()) {
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.IMAGE_JPEG); // Điều chỉnh kiểu ảnh tùy theo định dạng
                headers.setContentLength(photo.get().getContent().length);
                return new ResponseEntity<>(photo.get().getContent(),headers, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Photo By RealEstate Id
    @GetMapping("/photo")
    public ResponseEntity<List<CPhoto>> getPhotoByIdOfRealEstate(@RequestParam("realEstateId") long pRealEstateId) {
        try {
            Optional<CRealEstate> realEstateId = pRealEstateResponsitory.findById(pRealEstateId);
            if (realEstateId.isPresent()) {
                return new ResponseEntity<>(realEstateId.get().getPhoto(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Post Photo By RealEstate Id
    @PostMapping("/photo")
    public ResponseEntity<Object> postPhoto(@RequestBody MultipartFile[] pPhotoFiles,
            @RequestParam("realEstateId") long pRealEstateId) {
        try {
            Optional<CRealEstate> realStateId = pRealEstateResponsitory.findById(pRealEstateId);
            if (realStateId.isPresent()) {
                for (MultipartFile photoFile : pPhotoFiles) {
                    CPhoto photo = new CPhoto();
                    photo.setRealEstate(realStateId.get());
                    photo.setContent(photoFile.getBytes());
                    pPhotoResponsitory.save(photo);
                }
                return new ResponseEntity<>(HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete Photo By Id
    @DeleteMapping("/photo/{id}")
    public ResponseEntity<Object> deletePhotoById(@PathVariable("id") long pId) {
        try {
            Optional<CPhoto> photoId = pPhotoResponsitory.findById(pId);
            if (photoId.isPresent()) {
                pPhotoResponsitory.deleteById(pId);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // Delete All Photo
    @DeleteMapping("/photo")
    public  ResponseEntity<Object> deleteAllPhoto(){
        try {
            pPhotoResponsitory.deleteAll(); 
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // TODO: handle exception
              return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
