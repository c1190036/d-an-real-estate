package devcamp.thongnh.home24h.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import devcamp.thongnh.home24h.model.CAddress_Map;
import devcamp.thongnh.home24h.respository.IAddress_MapRespository;

@RestController 
@CrossOrigin
@RequestMapping
public class Address_MapController {
    @Autowired
    IAddress_MapRespository pAddress_MapRespository;

    //Get All Data Address_Map
    @GetMapping("/address-map")
    public ResponseEntity<List<CAddress_Map>> getAllAddressMap(){
        try {
            List<CAddress_Map> address_Maps= pAddress_MapRespository.findAll();
            return new ResponseEntity<>(address_Maps, HttpStatus.OK);
            
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //Get Address_Map By Id
    @GetMapping("/address-map/{id}")
    public ResponseEntity<CAddress_Map> getAddressById(@PathVariable("id") long pId){
        try {
            Optional<CAddress_Map> addressMapId= pAddress_MapRespository.findById(pId);
            if(addressMapId.isPresent()){
                return new ResponseEntity<>(addressMapId.get(), HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //Post Address_Map
    @PostMapping("/address-map")
    public ResponseEntity<Object> postAddressMap(@RequestBody CAddress_Map pAddress_Map){
        try {
            pAddress_MapRespository.save(pAddress_Map);
            return  new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception e) {
            // TODO: handle exception
             return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //Put Address_Map By Id
    @PutMapping("/address-map/{id}")
    public ResponseEntity<Object> putAddressByID(@PathVariable("id") long pId,@RequestBody CAddress_Map pAddress_Map)
    {
        try {
            Optional<CAddress_Map> addressMapId= pAddress_MapRespository.findById(pId);
            if(addressMapId.isPresent()){
                CAddress_Map address_Map=addressMapId.get();
                address_Map.setAddress(pAddress_Map.getAddress());
                address_Map.setLatitude(pAddress_Map.getLatitude());
                address_Map.setLongitude(pAddress_Map.getLongitude());
                pAddress_MapRespository.save(address_Map);
                return new ResponseEntity<>(address_Map, HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
             return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //Delete Address_Map By Id
    @DeleteMapping("/address-map/{id}")
    public ResponseEntity<Object> deleteAddressByID(@PathVariable("id") long pId)
    {
        try {
            Optional<CAddress_Map> addressMapId= pAddress_MapRespository.findById(pId);
            if(addressMapId.isPresent()){
                pAddress_MapRespository.deleteById(pId);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            else{
                 return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //Delete All Address_Map
    @DeleteMapping("/address-map")
    public ResponseEntity<Object> deleteAllAddressMap(){
        try {
            pAddress_MapRespository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            // TODO: handle exception
             return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
