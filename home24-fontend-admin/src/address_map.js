'use strict';
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gURL = "http://localhost:8080/address-map";
var gId = 0;
let gAddressMapTable = $("#address-map-table").DataTable({
  columns: [
    { data: "id" },
    { data: "address" },
    { data: "latitude" },
    { data: "longitude" },
    { data: 'action' },
  ],
  columnDefs: [
    {
      targets: -1,
      defaultContent: `<i class="fas fa-edit text-primary"></i>
      | <i class="fas fa-trash text-danger"></i>`,
    },
  ],
})

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$("#address-map-table").on("click", ".fa-edit", onUpdateAddressMapClick);
$("#address-map-table").on("click", ".fa-trash", onDeleteAddressMapClick);
$("#create-data").on("click", onPostAddressMapClick);
$("#save_data").on("click", onPutAddressMapClick);
$("#delete-address-map").on("click", onConfirmDeleteAddressMapClick);
$("#delete-all-address-map").on("click", onDeleteAllAddressMapClick);
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// hàm xử lý sự kiện tải trang
$(document).ready(function () {
  onLoadPage();
})
function onLoadPage() {
  gId = 0;
  $("#input-address-map").val("");
  $("#input-lat").val("");
  $("#input-lng").val("");
  $.get(gURL, loadAddressMapToTable);
}
function onUpdateAddressMapClick() {
  let vSelectRow = $(this).parents('tr');
  let vSelectedData = gAddressMapTable.row(vSelectRow).data();
  gId = vSelectedData.id;
  $.get(gURL + "/" + gId, loadAddressMapToInput);
}
function onDeleteAddressMapClick() {
  let vSelectRow = $(this).parents('tr');
  let vSelectedData = gAddressMapTable.row(vSelectRow).data();
  gId = vSelectedData.id;
  $("#modal-delete-address-map").modal('show');
}
function onPostAddressMapClick() {
  var vAddressMap = {
    address: "",
    latitude: 0.0,
    longitude: 0.0
  }
  getDataInput(vAddressMap);
  var vResult = validateAddressMap(vAddressMap);
  console.log(JSON.stringify(vAddressMap));
  if (vResult) {
    $.ajax({
      url: gURL,
      method: 'POST',
      data: JSON.stringify(vAddressMap),
      contentType: 'application/json',
      success: () => {
        alert('Address Map created successfully');
        onLoadPage();
      },
      error: (err) => alert(err.responseText),
    });
  }
}
function onPutAddressMapClick() {
  var vAddressMap = {
    address: "",
    latitude: 0.0,
    longitude: 0.0
  }
  getDataInput(vAddressMap);
  var vResult = validateAddressMap(vAddressMap);
  $.ajax({
    url: gURL+"/"+gId,
    method: 'PUT',
    data: JSON.stringify(vAddressMap),
    contentType: 'application/json',
    success: () => {
      alert('Address Map update successfully');
      onLoadPage();
    },
    error: (err) => alert(err.responseText),
  });
}
function onDeleteAllAddressMapClick() {
  gId = 0;
  $("#modal-delete-address-map").modal('show');
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/0
function loadAddressMapToTable(paramAddressMap) {
  gAddressMapTable.clear().rows.add(paramAddressMap).draw();
}
function loadAddressMapToInput(paramAddressMap) {
  console.log(paramAddressMap);
  gId = paramAddressMap.id;
  $("#input-address-map").val(paramAddressMap.address);
  $("#input-lat").val(paramAddressMap.latitude);
  $("#input-lng").val(paramAddressMap.longitude);
}
function getDataInput(paramAddressMap) {
  paramAddressMap.address = $("#input-address-map").val();
  paramAddressMap.latitude = $("#input-lat").val();
  paramAddressMap.longitude = $("#input-lng").val();
}
function validateAddressMap(paramAddressMap) {
  if (paramAddressMap.address == "" || paramAddressMap.latitude == "" || paramAddressMap.longitude == "") {
    alert("Không để trống dữ liệu trước khi save");
    return false;
  }
  else if (isNaN(paramAddressMap.latitude) || isNaN(paramAddressMap.longitude)) {
    alert("Vĩ độ và kinh độ phải là số");
    return false;
  }
  else {
    return true;
  }
}
function onConfirmDeleteAddressMapClick() {
  if (gId != 0) {
    $.ajax({
      url: gURL + "/" + gId,
      method: "DELETE",
      success: () => {
        alert("Address Map id: " + gId + " successfully delete");
        $('#modal-delete-address-map').modal('hide');
        onLoadPage();
      }
    })
  }
  else {
    $.ajax({
      url: gURL,
      method: "DELETE",
      success: () => {
        alert(" All Address Map successfully delete");
        $('#modal-delete-address-map').modal('hide');
        onLoadPage();
      }
    })
  }
}

