package devcamp.thongnh.home24h.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import devcamp.thongnh.home24h.model.CUtility;

public interface IUtilityResponsitory extends JpaRepository<CUtility,Long> {
    
}
