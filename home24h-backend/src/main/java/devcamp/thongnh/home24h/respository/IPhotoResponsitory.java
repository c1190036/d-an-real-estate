package devcamp.thongnh.home24h.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import devcamp.thongnh.home24h.model.CPhoto;

public interface IPhotoResponsitory  extends JpaRepository<CPhoto,Long>{
    
}
