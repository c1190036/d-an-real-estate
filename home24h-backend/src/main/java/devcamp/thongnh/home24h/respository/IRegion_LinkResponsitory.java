package devcamp.thongnh.home24h.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import devcamp.thongnh.home24h.model.CRegion_Link;

public interface IRegion_LinkResponsitory extends JpaRepository<CRegion_Link,Long> {
    
}
