package devcamp.thongnh.home24h.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import devcamp.thongnh.home24h.model.CStreet;

public interface IStreetRespository extends JpaRepository<CStreet,Long> {
    
}
