package devcamp.thongnh.home24h.respository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import devcamp.thongnh.home24h.model.CProvince;

public interface IProvinceRespository extends JpaRepository<CProvince, Long> {
    Optional<CProvince> findByCode(String code);
}
