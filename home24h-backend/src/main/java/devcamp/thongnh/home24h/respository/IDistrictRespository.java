package devcamp.thongnh.home24h.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import devcamp.thongnh.home24h.model.CDistrict;

public interface IDistrictRespository extends JpaRepository<CDistrict,Long > {
    
}
