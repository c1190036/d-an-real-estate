package devcamp.thongnh.home24h.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import devcamp.thongnh.home24h.model.CRealEstate;

public interface IRealEstateResponsitory extends JpaRepository<CRealEstate,Long> {
    
}
